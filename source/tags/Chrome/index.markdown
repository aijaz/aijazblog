---
layout: page
title: Tag&#58; Chrome
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/08/29/pinboard-search-engine/index.html">How to Make a Pinboard Search Engine Shortcut</a></h1>
<time datetime="2012-08-29T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>29</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

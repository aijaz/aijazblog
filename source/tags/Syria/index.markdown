---
layout: page
title: Tag&#58; Syria
footer: false
---

<div id="blog-archives" class="category">
<h2>2013</h2>

<article>
<h1><a href="/2013/08/30/dropping-napalm-bomb-on-school/index.html">Dropping a Napalm Bomb on a School</a></h1>
<time datetime="2013-08-30T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>30</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/opinion/'>Opinion</a></span>
</footer>
</article>
</div>

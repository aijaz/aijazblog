---
layout: page
title: Tag&#58; zsh
footer: false
---

<div id="blog-archives" class="category">
<h2>2013</h2>

<article>
<h1><a href="/2013/06/06/delete-multiple-versions-hockeyapp/index.html">How to Delete Multiple Versions of an App in HockeyApp</a></h1>
<time datetime="2013-06-06T00:00:00-06:00" pubdate><span class='month'>Jun</span> <span class='day'>06</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

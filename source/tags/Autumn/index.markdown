---
layout: page
title: Tag&#58; Autumn
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/10/02/when-autumn-leaves-start-to-fall/index.html">When Autumn Leaves Start to Fall</a></h1>
<time datetime="2012-10-02T00:00:00-06:00" pubdate><span class='month'>Oct</span> <span class='day'>02</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photos/'>Photos</a></span>
</footer>
</article>
</div>

---
layout: page
title: Tag&#58; DIY
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/07/15/making-a-kung-fu-wooden-dummy/index.html">Making a Kung Fu Wooden Dummy</a></h1>
<time datetime="2012-07-15T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>15</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/woodworking/'>Woodworking</a></span>
</footer>
</article>
</div>

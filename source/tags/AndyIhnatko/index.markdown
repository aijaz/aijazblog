---
layout: page
title: Tag&#58; AndyIhnatko
footer: false
---

<div id="blog-archives" class="category">
<h2>2015</h2>

<article>
<h1><a href="/2015/04/24/switching-from-consumer-to-creator/index.html">Switching From Being a Consumer to a Creator</a></h1>
<time datetime="2015-04-24T00:00:00-06:00" pubdate><span class='month'>Apr</span> <span class='day'>24</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

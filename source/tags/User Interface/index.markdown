---
layout: page
title: Tag&#58; User Interface
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/08/27/how-to-customize-octopress-theme/index.html">How to Customize Your Octopress Theme</a></h1>
<time datetime="2012-08-27T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>27</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2012/08/09/emotional-changes-to-ui/index.html">Emotional Changes to the User Interface</a></h1>
<time datetime="2012-08-09T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>09</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

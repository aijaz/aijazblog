---
layout: page
title: Tag&#58; AppleScript
footer: false
---

<div id="blog-archives" class="category">
<h2>2015</h2>

<article>
<h1><a href="/2015/02/17/viewing-omnigraffle-files-in-xcode/index.html">Viewing OmniGraffle Files in XCode</a></h1>
<time datetime="2015-02-17T00:00:00-06:00" pubdate><span class='month'>Feb</span> <span class='day'>17</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

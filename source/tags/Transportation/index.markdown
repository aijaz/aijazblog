---
layout: page
title: Tag&#58; Transportation
footer: false
---

<div id="blog-archives" class="category">
<h2>2015</h2>

<article>
<h1><a href="/2015/01/12/the-auto-rickshaw/index.html">The Auto Rickshaw</a></h1>
<time datetime="2015-01-12T00:00:00-06:00" pubdate><span class='month'>Jan</span> <span class='day'>12</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photos/'>Photos</a></span>
</footer>
</article>
</div>

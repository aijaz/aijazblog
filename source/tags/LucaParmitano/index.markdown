---
layout: page
title: Tag&#58; LucaParmitano
footer: false
---

<div id="blog-archives" class="category">
<h2>2013</h2>

<article>
<h1><a href="/2013/09/13/Luca-Parmitano/index.html">The Case of the Leaking Space Suit</a></h1>
<time datetime="2013-09-13T00:00:00-06:00" pubdate><span class='month'>Sep</span> <span class='day'>13</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/links/'>Links</a>, <a href='/categories/science/'>Science</a></span>
</footer>
</article>
</div>

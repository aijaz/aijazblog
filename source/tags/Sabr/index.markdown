---
layout: page
title: Tag&#58; Sabr
footer: false
---

<div id="blog-archives" class="category">
<h2>2014</h2>

<article>
<h1><a href="/2014/08/02/asking-allahs-help-gaza/index.html">Asking for Allah's Help for the People of Gaza</a></h1>
<time datetime="2014-08-02T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>02</span> <span class='year'>2014</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/opinion/'>Opinion</a></span>
</footer>
</article>
</div>

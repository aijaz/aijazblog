---
layout: page
title: Tag&#58; Apps
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/08/09/emotional-changes-to-ui/index.html">Emotional Changes to the User Interface</a></h1>
<time datetime="2012-08-09T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>09</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2010</h2>

<article>
<h1><a href="/2010/07/20/in-search-of-a-good-alarm-clock-app/index.html">In Search of a Good Alarm Clock App</a></h1>
<time datetime="2010-07-20T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>20</span> <span class='year'>2010</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a>, <a href='/categories/reviews/'>Reviews</a></span>
</footer>
</article>
</div>

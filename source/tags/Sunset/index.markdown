---
layout: page
title: Tag&#58; Sunset
footer: false
---

<div id="blog-archives" class="category">
<h2>2013</h2>

<article>
<h1><a href="/2013/10/09/leaving-glen-ellyn/index.html">Leaving Glen Ellyn</a></h1>
<time datetime="2013-10-09T00:00:00-06:00" pubdate><span class='month'>Oct</span> <span class='day'>09</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photos/'>Photos</a></span>
</footer>
</article>
</div>

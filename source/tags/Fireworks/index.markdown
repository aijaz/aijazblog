---
layout: page
title: Tag&#58; Fireworks
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/07/16/bokeh-fireworks/index.html">Bokeh Fireworks</a></h1>
<time datetime="2012-07-16T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>16</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photography/'>Photography</a></span>
</footer>
</article>
</div>

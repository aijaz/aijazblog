---
layout: page
title: Tag&#58; Octopress
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/10/01/octopress-as-a-photoblog/index.html">Using Octopress as a Photo Blog</a></h1>
<time datetime="2012-10-01T00:00:00-06:00" pubdate><span class='month'>Oct</span> <span class='day'>01</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2012/08/27/how-to-customize-octopress-theme/index.html">How to Customize Your Octopress Theme</a></h1>
<time datetime="2012-08-27T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>27</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2012/01/07/tag-clouds-with-octopress/index.html">Tag Clouds With Octopress</a></h1>
<time datetime="2012-01-07T00:00:00-06:00" pubdate><span class='month'>Jan</span> <span class='day'>07</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2011</h2>

<article>
<h1><a href="/2011/12/20/excluding-yourself-from-google-analytics/index.html">Excluding Yourself From Google Analytics</a></h1>
<time datetime="2011-12-20T00:00:00-06:00" pubdate><span class='month'>Dec</span> <span class='day'>20</span> <span class='year'>2011</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2011/12/12/switching-to-octopress/index.html">Switching to Octopress</a></h1>
<time datetime="2011-12-12T00:00:00-06:00" pubdate><span class='month'>Dec</span> <span class='day'>12</span> <span class='year'>2011</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

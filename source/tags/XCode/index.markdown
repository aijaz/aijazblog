---
layout: page
title: Tag&#58; XCode
footer: false
---

<div id="blog-archives" class="category">
<h2>2015</h2>

<article>
<h1><a href="/2015/02/17/viewing-omnigraffle-files-in-xcode/index.html">Viewing OmniGraffle Files in XCode</a></h1>
<time datetime="2015-02-17T00:00:00-06:00" pubdate><span class='month'>Feb</span> <span class='day'>17</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2014</h2>

<article>
<h1><a href="/2014/08/11/unit-testing-private-methods-xcode/index.html">Unit Testing Private Methods With XCode</a></h1>
<time datetime="2014-08-11T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>11</span> <span class='year'>2014</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

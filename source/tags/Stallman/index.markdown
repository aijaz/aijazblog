---
layout: page
title: Tag&#58; Stallman
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/07/01/not-as-expected/index.html">Not as Expected</a></h1>
<time datetime="2012-07-01T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>01</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2011</h2>

<article>
<h1><a href="/2011/11/21/there-and-back-again-a-hackers-switch-from-emacs-back-to-vi/index.html">There and Back Again - a Hacker's Switch From Emacs Back to Vi</a></h1>
<time datetime="2011-11-21T00:00:00-06:00" pubdate><span class='month'>Nov</span> <span class='day'>21</span> <span class='year'>2011</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

---
layout: page
title: Tag&#58; Productivity
footer: false
---

<div id="blog-archives" class="category">
<h2>2013</h2>

<article>
<h1><a href="/2013/09/10/sublime-text-links/index.html">Links From My Sublime Text Talk</a></h1>
<time datetime="2013-09-10T00:00:00-06:00" pubdate><span class='month'>Sep</span> <span class='day'>10</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2013/06/06/delete-multiple-versions-hockeyapp/index.html">How to Delete Multiple Versions of an App in HockeyApp</a></h1>
<time datetime="2013-06-06T00:00:00-06:00" pubdate><span class='month'>Jun</span> <span class='day'>06</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2012</h2>

<article>
<h1><a href="/2012/08/29/pinboard-search-engine/index.html">How to Make a Pinboard Search Engine Shortcut</a></h1>
<time datetime="2012-08-29T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>29</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2011</h2>

<article>
<h1><a href="/2011/12/02/how-to-quickly-delete-words-while-in-insert-mode-in-vim/index.html">How to Quickly Delete Words While in Insert Mode in Vim</a></h1>
<time datetime="2011-12-02T00:00:00-06:00" pubdate><span class='month'>Dec</span> <span class='day'>02</span> <span class='year'>2011</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2011/11/28/how-to-insert-a-line-of-dashes-in-vim/index.html">How to Insert a Line of Dashes in Vim</a></h1>
<time datetime="2011-11-28T00:00:00-06:00" pubdate><span class='month'>Nov</span> <span class='day'>28</span> <span class='year'>2011</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2010</h2>

<article>
<h1><a href="/2010/07/17/processing-a-list-of-files-in-bash/index.html">Processing a List of Files in Bash</a></h1>
<time datetime="2010-07-17T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>17</span> <span class='year'>2010</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2010/04/03/firefox-search-shortcut/index.html">Firefox Search Shortcut</a></h1>
<time datetime="2010-04-03T00:00:00-06:00" pubdate><span class='month'>Apr</span> <span class='day'>03</span> <span class='year'>2010</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2010/02/20/navigating-the-directory-stack-in-bash/index.html">Navigating the Directory Stack in 'Bash'</a></h1>
<time datetime="2010-02-20T00:00:00-06:00" pubdate><span class='month'>Feb</span> <span class='day'>20</span> <span class='year'>2010</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2010/01/26/why-text-editors-matter/index.html">Why Text Editors Matter</a></h1>
<time datetime="2010-01-26T00:00:00-06:00" pubdate><span class='month'>Jan</span> <span class='day'>26</span> <span class='year'>2010</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

---
layout: page
title: Tag&#58; mod_perl
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/07/14/installing-modperl2/index.html">Installing Mod_perl 2.0</a></h1>
<time datetime="2012-07-14T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>14</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

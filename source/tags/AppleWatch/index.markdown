---
layout: page
title: Tag&#58; AppleWatch
footer: false
---

<div id="blog-archives" class="category">
<h2>2015</h2>

<article>
<h1><a href="/2015/04/28/apple-watch-energy/index.html">Diary of Apple Watch Energy Consumption</a></h1>
<time datetime="2015-04-28T00:00:00-06:00" pubdate><span class='month'>Apr</span> <span class='day'>28</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2015/04/26/apple-watch-layout/index.html">The Layout of My Apple Watch Apps</a></h1>
<time datetime="2015-04-26T00:00:00-06:00" pubdate><span class='month'>Apr</span> <span class='day'>26</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

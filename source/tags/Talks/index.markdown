---
layout: page
title: Tag&#58; Talks
footer: false
---

<div id="blog-archives" class="category">
<h2>2013</h2>

<article>
<h1><a href="/2013/09/10/sublime-text-links/index.html">Links From My Sublime Text Talk</a></h1>
<time datetime="2013-09-10T00:00:00-06:00" pubdate><span class='month'>Sep</span> <span class='day'>10</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

---
layout: page
title: Tag&#58; Flash
footer: false
---

<div id="blog-archives" class="category">
<h2>2014</h2>

<article>
<h1><a href="/2014/02/01/recreating-david-hobbys-profile-pic/index.html">Recreating David Hobby's Profile Picture</a></h1>
<time datetime="2014-02-01T00:00:00-06:00" pubdate><span class='month'>Feb</span> <span class='day'>01</span> <span class='year'>2014</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photography/'>Photography</a></span>
</footer>
</article>

<article>
<h1><a href="/2014/02/01/external-flash-Fuji-X100S/index.html">External Flashes With the Fuji X100S</a></h1>
<time datetime="2014-02-01T00:00:00-06:00" pubdate><span class='month'>Feb</span> <span class='day'>01</span> <span class='year'>2014</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photography/'>Photography</a></span>
</footer>
</article>
<h2>2012</h2>

<article>
<h1><a href="/2012/03/29/running-out-of-power-at-the-grand-canyon/index.html">Running Out of Power at the Grand Canyon</a></h1>
<time datetime="2012-03-29T00:00:00-06:00" pubdate><span class='month'>Mar</span> <span class='day'>29</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photography/'>Photography</a></span>
</footer>
</article>
</div>

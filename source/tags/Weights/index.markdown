---
layout: page
title: Tag&#58; Weights
footer: false
---

<div id="blog-archives" class="category">
<h2>2014</h2>

<article>
<h1><a href="/2014/10/19/good-spotter/index.html">The Importance of Having a Good Spotter</a></h1>
<time datetime="2014-10-19T00:00:00-06:00" pubdate><span class='month'>Oct</span> <span class='day'>19</span> <span class='year'>2014</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/fitness/'>Fitness</a></span>
</footer>
</article>
</div>

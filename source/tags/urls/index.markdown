---
layout: page
title: Tag&#58; urls
footer: false
---

<div id="blog-archives" class="category">
<h2>2013</h2>

<article>
<h1><a href="/2013/07/16/a-list-of-url-schemes/index.html">A List of iOS URL Schemes</a></h1>
<time datetime="2013-07-16T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>16</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

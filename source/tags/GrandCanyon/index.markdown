---
layout: page
title: Tag&#58; GrandCanyon
footer: false
---

<div id="blog-archives" class="category">
<h2>2012</h2>

<article>
<h1><a href="/2012/08/31/horseshoe-bend/index.html">Horseshoe Bend</a></h1>
<time datetime="2012-08-31T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>31</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photos/'>Photos</a></span>
</footer>
</article>
</div>

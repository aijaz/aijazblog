---
layout: page
title: Tag&#58; Literals
footer: false
---

<div id="blog-archives" class="category">
<h2>2013</h2>

<article>
<h1><a href="/2013/05/06/evolution-of-objc-database-code/index.html">Evolution of Objective C Database Code</a></h1>
<time datetime="2013-05-06T00:00:00-06:00" pubdate><span class='month'>May</span> <span class='day'>06</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

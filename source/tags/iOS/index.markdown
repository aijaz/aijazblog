---
layout: page
title: Tag&#58; iOS
footer: false
---

<div id="blog-archives" class="category">
<h2>2015</h2>

<article>
<h1><a href="/2015/05/17/fmdb-performance-improvement/index.html">Performance Improvement With FMDB</a></h1>
<time datetime="2015-05-17T00:00:00-06:00" pubdate><span class='month'>May</span> <span class='day'>17</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2015/02/17/viewing-omnigraffle-files-in-xcode/index.html">Viewing OmniGraffle Files in XCode</a></h1>
<time datetime="2015-02-17T00:00:00-06:00" pubdate><span class='month'>Feb</span> <span class='day'>17</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2014</h2>

<article>
<h1><a href="/2014/08/11/unit-testing-private-methods-xcode/index.html">Unit Testing Private Methods With XCode</a></h1>
<time datetime="2014-08-11T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>11</span> <span class='year'>2014</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2013</h2>

<article>
<h1><a href="/2013/07/16/a-list-of-url-schemes/index.html">A List of iOS URL Schemes</a></h1>
<time datetime="2013-07-16T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>16</span> <span class='year'>2013</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2012</h2>

<article>
<h1><a href="/2012/08/09/emotional-changes-to-ui/index.html">Emotional Changes to the User Interface</a></h1>
<time datetime="2012-08-09T00:00:00-06:00" pubdate><span class='month'>Aug</span> <span class='day'>09</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2011</h2>

<article>
<h1><a href="/2011/04/25/implementing-inertial-scrolling-in-ios/index.html">Implementing Inertial Scrolling in iOS</a></h1>
<time datetime="2011-04-25T00:00:00-06:00" pubdate><span class='month'>Apr</span> <span class='day'>25</span> <span class='year'>2011</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2010</h2>

<article>
<h1><a href="/2010/09/19/hello-world-on-the-iphone/index.html">"Hello, World" on the iPhone</a></h1>
<time datetime="2010-09-19T00:00:00-06:00" pubdate><span class='month'>Sep</span> <span class='day'>19</span> <span class='year'>2010</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

---
layout: page
title: Tag&#58; Twitter
footer: false
---

<div id="blog-archives" class="category">
<h2>2015</h2>

<article>
<h1><a href="/2015/04/24/switching-from-consumer-to-creator/index.html">Switching From Being a Consumer to a Creator</a></h1>
<time datetime="2015-04-24T00:00:00-06:00" pubdate><span class='month'>Apr</span> <span class='day'>24</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>

<article>
<h1><a href="/2015/01/23/about-that-antelope-canyon-shot/index.html">About That Antelope Canyon Shot</a></h1>
<time datetime="2015-01-23T00:00:00-06:00" pubdate><span class='month'>Jan</span> <span class='day'>23</span> <span class='year'>2015</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/photography/'>Photography</a></span>
</footer>
</article>
<h2>2012</h2>

<article>
<h1><a href="/2012/07/01/not-as-expected/index.html">Not as Expected</a></h1>
<time datetime="2012-07-01T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>01</span> <span class='year'>2012</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
<h2>2011</h2>

<article>
<h1><a href="/2011/07/09/taking-back-my-relationships/index.html">Taking Back My Relationships</a></h1>
<time datetime="2011-07-09T00:00:00-06:00" pubdate><span class='month'>Jul</span> <span class='day'>09</span> <span class='year'>2011</span></time>
<footer>
<span class="categories">posted in 
<a href='/categories/computers/'>Computers</a></span>
</footer>
</article>
</div>

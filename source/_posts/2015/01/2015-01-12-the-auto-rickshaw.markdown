---
published: true
layout: photo
title: "The Auto Rickshaw"
date: 2015-01-12 11:10
comments: false
categories:
- Photos
author: Aijaz Ansari
image: /images/photos/VacEnd2014/AJAZ1030.jpg
description: Zipping through the streets of Bangalore in an auto rickshaw.
aperture: 4.0
camera: X-T1
creator: Aijaz Ansari
dateTaken: 2015:01:06 17:54:10
flash: Off, Did not fire
focalLength: 10.0 mm
histogram: /images/photos/VacEnd2014/AJAZ1030_hist.png
histogramHeight: 50
histogramWidth: 128
iso: 6400
lens: "10-24mm f/4"
license: <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a>
photoHeight: 1632
photoWidth: 2448
scaleFactor: 1.5
shutterSpeed: 1/20
tags: 
- VacEnd2014
- Transportation
- Bangalore
thumbnail: /images/photos/VacEnd2014/AJAZ1030_tn.jpg
thumbnailHeight: 256
thumbnailWidth: 384
---

Zipping through the streets of Bangalore in an auto rickshaw.

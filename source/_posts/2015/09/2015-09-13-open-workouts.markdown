---
layout: post
title: "Open Workouts"
date: 2015-09-13 17:30
comments: false
categories:
- 30days
- Fitness
author: Aijaz Ansari
tags:
- AppleWatch
- Exercise
- 30days
description: "This is one of my favorite things about the Workout App on the Apple Watch"
---

<!-- l /images/2015/09/openWalkFace.png No Specific Goals -->

This is one of my favorite things about the Workout App on the Apple Watch: the ability to start a workout without having any specific goal in mind. 


I love that I can just start and see where the workout takes me. There's no pressure reach a certain time or calorie count. I can just play it by ear.

See you tomorrow.

_This is the 13th of my [30 days][] posts._

[30 days]: /2015/08/31/30-days/

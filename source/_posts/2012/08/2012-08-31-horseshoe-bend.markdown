---
layout: photo
title: "Horseshoe Bend"
date: 2012-08-31 17:51
comments: false
categories:
- Photos
author: Aijaz Ansari
thumbnail: /images/photos/arizona/P_JAZ4129_tn.jpg
tags:
- Arizona
- GrandCanyon
- HorseshoeBend
image: /images/photos/arizona/P_JAZ4129.jpg
histogram: /images/photos/arizona/P_JAZ4129_hist.png
histogramWidth: 128
histogramHeight: 50
thumbnailHeight: 256
creator: Aijaz Ansari
copyright: Copyright 2012 Aijaz Ansari
lens: "AF-S VR Zoom-Nikkor 24-120mm f/3.5-5.6G IF-ED"
shutterSpeed: 1/320
photoWidth: 768
thumbnailWidth: 384
iso: 200
flash: No Flash
photoHeight: 511
focalLength: 24.0 mm
scaleFactor: 1.0
aperture: 9.0
camera: NIKON D700
license: <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png" /></a>
dateTaken: 2012:03:29 17:35:23
description: The Horseshoe bend is a bend of the Colorado River located near Page, Arizona. 
---

This is a bend of the Colorado River located near Page, Arizona. In Page
you can find the famous
[Antelope Canyons](/2012/09/02/antelope-canyon/).

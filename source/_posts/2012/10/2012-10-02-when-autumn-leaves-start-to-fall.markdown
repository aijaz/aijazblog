---
layout: photo
title: "When Autumn Leaves Start to Fall"
date: 2012-10-02 09:00
comments: false
categories:
- Photos
author: Aijaz Ansari
image: /images/photos/autumnOak/autumnOak_6208.jpg
aperture: 1.8
camera: NIKON D700
copyright: Copyright 2010 Aijaz Ansari
creator: Aijaz Ansari
dateTaken: 2012:09:29 17:33:26
flash: No Flash
focalLength: 50.0 mm
histogram: /images/photos/autumnOak/autumnOak_6208_hist.png
histogramHeight: 50
histogramWidth: 128
iso: 200
lens: "AF Nikkor 50mm f/1.8D"
license: <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a>
photoHeight: 511
photoWidth: 768
scaleFactor: 1.0
shutterSpeed: 1/6400
tags: 
- Autumn
thumbnail: /images/photos/autumnOak/autumnOak_6208_tn.jpg
thumbnailHeight: 256
thumbnailWidth: 384
description: This is an Oak tree that I planted in my front yard about five years ago.
---

This is an Oak tree that I planted in my front yard about five
years ago.  I'm glad it's survived all the storms we've had since then. I
hope to see it grow into a Mighty Oak.

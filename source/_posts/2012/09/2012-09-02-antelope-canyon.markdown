---
layout: photo
title: "Antelope Canyon"
date: 2012-09-02 11:30
comments: false
categories:
- Photos
author: Aijaz Ansari
image: /images/photos/arizona/P_JAZ4109.jpg
aperture: 4.8
camera: NIKON D700
copyright: Copyright 2012 Aijaz Ansari
creator: Aijaz Ansari
dateTaken: 2012:03:29 16:21:19
flash: No Flash
focalLength: 50.0 mm
histogram: /images/photos/arizona/P_JAZ4109_hist.png
histogramWidth: 128
histogramHeight: 50
iso: 200
lens: "AF-S VR Zoom-Nikkor 24-120mm f/3.5-5.6G IF-ED"
license: <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/80x15.png" /></a>
photoHeight: 511
photoWidth: 340
scaleFactor: 1.0
shutterSpeed: 0.4
tags: 
- Arizona
- AntelopeCanyon
thumbnail: /images/photos/arizona/P_JAZ4109_tn.jpg
thumbnailHeight: 384
thumbnailWidth: 255
description: The Lower Antelope Canyon in Page, Arizona is a place of breathtaking beauty.
---

The Lower Antelope Canyon in Page, Arizona is a place of breathtaking
beauty.  Located in Navajo country, it's best viewed at around noon when
the angle of the sun's rays makes for very dramatic lighting.

---
layout: photo
title: "Leaving Glen Ellyn"
date: 2013-10-09 08:35
comments: false
categories:
- Photos
author: Aijaz Ansari
image: /images/2013/10/metra.jpg
aperture: 5.6
camera: X100S
creator: Aijaz Ansari
dateTaken: 2013:10:04 18:13:38
flash: Off, Did not fire
focalLength: 23.0 mm
histogram: /images/2013/10/metra_hist.png
histogramHeight: 50
histogramWidth: 128
iso: 400
license: <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a>
photoHeight: 1380
photoWidth: 2448
scaleFactor: 3.2
shutterSpeed: 1/250
tags: 
- Chicago
- Trains
- Commute
- Sunset
thumbnail: /images/2013/10/metra_tn.jpg
thumbnailHeight: 216
thumbnailWidth: 384
description: The Metra leaving Glen Ellyn as daylight comes to an end
---

I like how the rays of the sun reflected off the train.  I only had a second to take this picture as there was a crowd of commuters right behind me. 

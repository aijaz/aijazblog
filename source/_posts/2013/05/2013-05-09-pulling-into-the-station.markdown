---
layout: photo
title: "Pulling into the Station"
date: 2013-05-09 17:30
comments: false
categories:
- Photos
author: Aijaz Ansari
image: /images/2013/05/09/metra.jpg
aperture: 2.0
camera: X100S
creator: Aijaz Ansari
dateTaken: 2013:05:09 17:27:00
expComp: -0.67
flash: Off, Did not fire
focalLength: 23.0 mm
histogram: /images/2013/05/09/metra_hist.png
histogramHeight: 50
histogramWidth: 128
iso: 800
license: <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a>
photoHeight: 1380
photoWidth: 2448
scaleFactor: 3.2
shutterSpeed: 1/20
tags: 
- Chicago
- Trains
- Commute
thumbnail: /images/2013/05/09/metra_tn.jpg
thumbnailHeight: 216
thumbnailWidth: 384
description: Taken by my new Fuji X100S
---


This photo was taken in 'stealth' mode: shooting from the hip, no audio,
no display on the rear LCD.  I had no idea what the composition would look
like.  

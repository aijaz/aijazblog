---
layout: page
title: "iOS URL Schemes"
date: 2013-07-16 00:00
comments: false
sharing: true
footer: true
---

When I wanted to add a URL scheme to my iOS app I searched for a registry
of URL schemes to see whether the one I wanted (```quran://```) was
already being used or not.  I found two good registries:
[HandleOpenURL](http://handleOpenUrl.com) and
[IPhone URL Schemes](http://wiki.akosma.com/IPhone_URL_Schemes).
Unfortunately, the latter no longer appears to be mantained, because
[Akosma shut down its operations](http://akosma.com/2013/05/22/shutting-down/) in May 2013.

Since the content on their Wiki was licensed under the
<a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" class="external " title="http://creativecommons.org/licenses/by-nc-sa/3.0/">Attribution-Noncommercial-Share Alike 3.0 Unported</a>
CC license, I copied the content here to preserve it in case the Akosma page goes down.

I'm presenting this content under the same license, and am accepting
submissions. If you want to add your urlscheme to this list, please email
```urlschemelist``` at ```euclidsoftware.com``` with the following information and I'll add it to the list:

* the name of your app
* the url scheme
* a short description how the url is interpreted

No marketing text, please.  This won't be a place to pitch your app.
Please don't send code samples any more - It's a problem if the samples
are out of date or wrong.

I'm not trying to make any money off this, so please accept this as a
volunteer effort.  And please don't forget to submit your urlscheme to
[HandleOpenURL](http://handleOpenUrl.com) as well.

Thanks,

Aijaz.


<div id="bodyContent">
			<h3 id="siteSub">From akosma wiki</h3>
			<div id="contentSub"></div>
									
			<p>This page centralizes code samples for URL schemes
			available in many iPhone applications, not only in Apple's but
			in many others. It also includes programming tips and
			references about implementing apps registering or consuming
			URL schemes.
            </p>


<table id="toc" class="toc" summary="Contents"><tbody><tr><td><div id="toctitle"><h2>Contents</h2> </div>
<ul>
<li class="toclevel-1"><a href="#Terms_of_Use"><span class="tocnumber">1</span> <span class="toctext">Terms of Use</span></a></li>
<li class="toclevel-1"><a href="#Programming_Tips"><span class="tocnumber">2</span> <span class="toctext">Programming Tips</span></a>
<ul>
<li class="toclevel-2"><a href="#Registering_your_own_URL_schemes"><span class="tocnumber">2.1</span> <span class="toctext">Registering your own URL schemes</span></a></li>
<li class="toclevel-2"><a href="#Note_about_URL_Encoding_in_Objective-C"><span class="tocnumber">2.2</span> <span class="toctext">Note about URL Encoding in Objective-C</span></a></li>
</ul>
</li>
<li class="toclevel-1"><a href="#Apple_iPhone_Applications"><span class="tocnumber">3</span> <span class="toctext">Apple iPhone Applications</span></a>
<ul>
<li class="toclevel-2"><a href="#Safari"><span class="tocnumber">3.1</span> <span class="toctext">Safari</span></a></li>
<li class="toclevel-2"><a href="#Maps"><span class="tocnumber">3.2</span> <span class="toctext">Maps</span></a></li>
<li class="toclevel-2"><a href="#Phone"><span class="tocnumber">3.3</span> <span class="toctext">Phone</span></a></li>
<li class="toclevel-2"><a href="#SMS"><span class="tocnumber">3.4</span> <span class="toctext">SMS</span></a></li>
<li class="toclevel-2"><a href="#Mail"><span class="tocnumber">3.5</span> <span class="toctext">Mail</span></a></li>
<li class="toclevel-2"><a href="#Music_.2F_iPod"><span class="tocnumber">3.6</span> <span class="toctext">Music / iPod</span></a></li>
<li class="toclevel-2"><a href="#YouTube"><span class="tocnumber">3.7</span> <span class="toctext">YouTube</span></a></li>
<li class="toclevel-2"><a href="#Videos"><span class="tocnumber">3.8</span> <span class="toctext">Videos</span></a></li>
<li class="toclevel-2"><a href="#iTunes"><span class="tocnumber">3.9</span> <span class="toctext">iTunes</span></a></li>
<li class="toclevel-2"><a href="#App_Store"><span class="tocnumber">3.10</span> <span class="toctext">App Store</span></a></li>
<li class="toclevel-2"><a href="#iBooks"><span class="tocnumber">3.11</span> <span class="toctext">iBooks</span></a></li>
</ul>
</li>
<li class="toclevel-1"><a href="#Third_Party_Applications"><span class="tocnumber">4</span> <span class="toctext">Third Party Applications</span></a>
<ul>
<li class="toclevel-2"><a href="#AirSharing"><span class="tocnumber">4.1</span> <span class="toctext">AirSharing</span></a></li>
<li class="toclevel-2"><a href="#Alocola"><span class="tocnumber">4.2</span> <span class="toctext">Alocola</span></a></li>
<li class="toclevel-2"><a href="#Ambiance"><span class="tocnumber">4.3</span> <span class="toctext">Ambiance</span></a></li>
<li class="toclevel-2"><a href="#Appigo_Notebook"><span class="tocnumber">4.4</span> <span class="toctext">Appigo Notebook</span></a></li>
<li class="toclevel-2"><a href="#Appigo_Todo"><span class="tocnumber">4.5</span> <span class="toctext">Appigo Todo</span></a></li>
<li class="toclevel-2"><a href="#Bookmarkers_.2F_Bookmarkers_Pro"><span class="tocnumber">4.6</span> <span class="toctext">Bookmarkers / Bookmarkers Pro</span></a></li>
<li class="toclevel-2"><a href="#Call_Recorder_and_Cheap_Calls"><span class="tocnumber">4.7</span> <span class="toctext">Call Recorder and Cheap Calls</span></a></li>
<li class="toclevel-2"><a href="#Duo"><span class="tocnumber">4.8</span> <span class="toctext">Duo</span></a></li>
<li class="toclevel-2"><a href="#The_Cartographer"><span class="tocnumber">4.9</span> <span class="toctext">The Cartographer</span></a></li>
<li class="toclevel-2"><a href="#ChatCo"><span class="tocnumber">4.10</span> <span class="toctext">ChatCo</span></a></li>
<li class="toclevel-2"><a href="#Cobi_Tools"><span class="tocnumber">4.11</span> <span class="toctext">Cobi Tools</span></a></li>
<li class="toclevel-2"><a href="#Credit_Card_Terminal"><span class="tocnumber">4.12</span> <span class="toctext">Credit Card Terminal</span></a></li>
<li class="toclevel-2"><a href="#DailyMotion"><span class="tocnumber">4.13</span> <span class="toctext">DailyMotion</span></a></li>
<li class="toclevel-2"><a href="#Darkslide_.28formerly_Exposure.29"><span class="tocnumber">4.14</span> <span class="toctext">Darkslide (formerly Exposure)</span></a></li>
<li class="toclevel-2"><a href="#Echofon_.2F_Echofon_Pro_.28formerly_Twitterfon_.2F_Twitterfon_Pro.29"><span class="tocnumber">4.15</span> <span class="toctext">Echofon / Echofon Pro (formerly Twitterfon / Twitterfon Pro)</span></a></li>
<li class="toclevel-2"><a href="#Eureka"><span class="tocnumber">4.16</span> <span class="toctext">Eureka</span></a></li>
<li class="toclevel-2"><a href="#Explorimmo_immobilier"><span class="tocnumber">4.17</span> <span class="toctext">Explorimmo immobilier</span></a></li>
<li class="toclevel-2"><a href="#Facebook"><span class="tocnumber">4.18</span> <span class="toctext">Facebook</span></a></li>
<li class="toclevel-2"><a href="#Geocaching_Buddy_.28_GCBuddy_.29"><span class="tocnumber">4.19</span> <span class="toctext">Geocaching Buddy ( GCBuddy )</span></a></li>
<li class="toclevel-2"><a href="#Geopher_Lite"><span class="tocnumber">4.20</span> <span class="toctext">Geopher Lite</span></a></li>
<li class="toclevel-2"><a href="#Google_Chrome"><span class="tocnumber">4.21</span> <span class="toctext">Google Chrome</span></a></li>
<li class="toclevel-2"><a href="#Google_Earth"><span class="tocnumber">4.22</span> <span class="toctext">Google Earth</span></a></li>
<li class="toclevel-2"><a href="#Image_Data"><span class="tocnumber">4.23</span> <span class="toctext">Image Data</span></a></li>
<li class="toclevel-2"><a href="#IMDb_Movies_.26_TV"><span class="tocnumber">4.24</span> <span class="toctext">IMDb Movies &amp; TV</span></a></li>
<li class="toclevel-2"><a href="#INRIX_Traffic"><span class="tocnumber">4.25</span> <span class="toctext">INRIX Traffic</span></a></li>
<li class="toclevel-2"><a href="#iTranslate_.7E_the_universal_translator"><span class="tocnumber">4.26</span> <span class="toctext">iTranslate ~ the universal translator</span></a></li>
<li class="toclevel-2"><a href="#Junos_Pulse_VPN_app_for_iOS"><span class="tocnumber">4.27</span> <span class="toctext">Junos Pulse VPN app for iOS</span></a></li>
<li class="toclevel-2"><a href="#Loan_Plan_-_Amortization_Calculator"><span class="tocnumber">4.28</span> <span class="toctext">Loan Plan - Amortization Calculator</span></a></li>
<li class="toclevel-2"><a href="#Navigon"><span class="tocnumber">4.29</span> <span class="toctext">Navigon</span></a></li>
<li class="toclevel-2"><a href="#Notitas"><span class="tocnumber">4.30</span> <span class="toctext">Notitas</span></a></li>
<li class="toclevel-2"><a href="#OpenTable"><span class="tocnumber">4.31</span> <span class="toctext">OpenTable</span></a></li>
<li class="toclevel-2"><a href="#Opera_Mini"><span class="tocnumber">4.32</span> <span class="toctext">Opera Mini</span></a></li>
<li class="toclevel-2"><a href="#pic2shop"><span class="tocnumber">4.33</span> <span class="toctext">pic2shop</span></a></li>
<li class="toclevel-2"><a href="#Pinterest"><span class="tocnumber">4.34</span> <span class="toctext">Pinterest</span></a></li>
<li class="toclevel-2"><a href="#Pocketpedia"><span class="tocnumber">4.35</span> <span class="toctext">Pocketpedia</span></a></li>
<li class="toclevel-2"><a href="#Portscan"><span class="tocnumber">4.36</span> <span class="toctext">Portscan</span></a></li>
<li class="toclevel-2"><a href="#PureCalc"><span class="tocnumber">4.37</span> <span class="toctext">PureCalc</span></a></li>
<li class="toclevel-2"><a href="#Quran"><span class="tocnumber">4.375</span> <span class="toctext">Quran</span></a></li>
<li class="toclevel-2"><a href="#Round_Tuit"><span class="tocnumber">4.38</span> <span class="toctext">Round Tuit</span></a></li>
<li class="toclevel-2"><a href="#Ships.C2.B2_Viewer"><span class="tocnumber">4.39</span> <span class="toctext">Ships² Viewer</span></a></li>
<li class="toclevel-2"><a href="#Site_Check"><span class="tocnumber">4.40</span> <span class="toctext">Site Check</span></a></li>
<li class="toclevel-2"><a href="#Surfboard_.28iPad.29"><span class="tocnumber">4.41</span> <span class="toctext">Surfboard (iPad)</span></a></li>
<li class="toclevel-2"><a href="#TimeLog"><span class="tocnumber">4.42</span> <span class="toctext">TimeLog</span></a></li>
<li class="toclevel-2"><a href="#Timer"><span class="tocnumber">4.43</span> <span class="toctext">Timer</span></a></li>
<li class="toclevel-2"><a href="#TomTom"><span class="tocnumber">4.44</span> <span class="toctext">TomTom</span></a></li>
<li class="toclevel-2"><a href="#Tweetbot"><span class="tocnumber">4.45</span> <span class="toctext">Tweetbot</span></a></li>
<li class="toclevel-2"><a href="#Tweetie"><span class="tocnumber">4.46</span> <span class="toctext">Tweetie</span></a></li>
<li class="toclevel-2"><a href="#Twittelator_Pro"><span class="tocnumber">4.47</span> <span class="toctext">Twittelator Pro</span></a></li>
<li class="toclevel-2"><a href="#Twitter"><span class="tocnumber">4.48</span> <span class="toctext">Twitter</span></a></li>
<li class="toclevel-2"><a href="#Twitterriffic_.2F_Twitterriffic_Premium"><span class="tocnumber">4.49</span> <span class="toctext">Twitterriffic / Twitterriffic Premium</span></a></li>
<li class="toclevel-2"><a href="#Unfragment"><span class="tocnumber">4.50</span> <span class="toctext">Unfragment</span></a></li>
<li class="toclevel-2"><a href="#Wikiamo"><span class="tocnumber">4.51</span> <span class="toctext">Wikiamo</span></a></li>
<li class="toclevel-2"><a href="#Yummy"><span class="tocnumber">4.52</span> <span class="toctext">Yummy</span></a></li>
<li class="toclevel-2"><a href="#Swiss_Phone_Book_by_local.ch_.28from_version_1.5.29"><span class="tocnumber">4.53</span> <span class="toctext">Swiss Phone Book by local.ch (from version 1.5)</span></a></li>
<li class="toclevel-2"><a href="#Seesmic"><span class="tocnumber">4.54</span> <span class="toctext">Seesmic</span></a></li>
<li class="toclevel-2"><a href="#Offline_Pages"><span class="tocnumber">4.55</span> <span class="toctext">Offline Pages</span></a></li>
<li class="toclevel-2"><a href="#Terminology"><span class="tocnumber">4.56</span> <span class="toctext">Terminology</span></a></li>
<li class="toclevel-2"><a href="#ZenTap"><span class="tocnumber">4.57</span> <span class="toctext">ZenTap</span></a></li>
<li class="toclevel-2"><a href="#ZenTap_Pro"><span class="tocnumber">4.58</span> <span class="toctext">ZenTap Pro</span></a></li>
<li class="toclevel-2"><a href="#TextFaster"><span class="tocnumber">4.59</span> <span class="toctext">TextFaster</span></a></li>
<li class="toclevel-2"><a href="#Threadnote"><span class="tocnumber">4.60</span> <span class="toctext">Threadnote</span></a></li>
<li class="toclevel-2"><a href="#TikiSurf"><span class="tocnumber">4.61</span> <span class="toctext">TikiSurf</span></a></li>
<li class="toclevel-2"><a href="#Waze"><span class="tocnumber">4.62</span> <span class="toctext">Waze</span></a></li>
<li class="toclevel-2"><a href="#Webmail.2B"><span class="tocnumber">4.63</span> <span class="toctext">Webmail+</span></a></li>
<li class="toclevel-2"><a href="#Where_To.3F"><span class="tocnumber">4.64</span> <span class="toctext">Where To?</span></a></li>
<li class="toclevel-2"><a href="#iWavit_Tabula_Rasa"><span class="tocnumber">4.65</span> <span class="toctext">iWavit Tabula Rasa</span></a></li>
</ul>
</li>
</ul>
</td></tr></tbody></table><script type="text/javascript"> if (window.showTocToggle) { var tocShowText = "show"; var tocHideText = "hide"; showTocToggle(); } </script>
<a name="Terms_of_Use" id="Terms_of_Use"></a><h2> <span class="mw-headline"> Terms of Use </span></h2>
<p>This page is a courtesy service. If you want your application to appear here, please follow the instructions shown at the top of the page. 
</p><p>Thanks for sharing your URL scheme to the community!
</p>
<a name="Programming_Tips" id="Programming_Tips"></a><h2> <span class="mw-headline"> Programming Tips </span></h2>
<a name="Registering_your_own_URL_schemes" id="Registering_your_own_URL_schemes"></a><h3> <span class="mw-headline"> Registering your own URL schemes </span></h3>
<p>Please check <a href="http://iphonedevelopertips.com/cocoa/launching-your-own-application-via-a-custom-url-scheme.html" class="external text" title="http://iphonedevelopertips.com/cocoa/launching-your-own-application-via-a-custom-url-scheme.html" rel="nofollow">this page in iPhone Developer Tips</a>.
</p>
<a name="Note_about_URL_Encoding_in_Objective-C" id="Note_about_URL_Encoding_in_Objective-C"></a><h3> <span class="mw-headline"> Note about URL Encoding in Objective-C </span></h3>
<p>There can be some problems with <b>NSString's stringByAddingPercentEscapesUsingEncoding:</b> method since it does not encode some reserved characters for URLs; you might prefer to use this code instead:
</p>
<pre>CFStringRef encoded = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, 
                                                              (CFStringRef)textToEncode,
                                                              NULL, 
                                                              (CFStringRef)@";/?:@&amp;=+$,", 
                                                              kCFStringEncodingUTF8);
</pre>
<p>The Core Foundation CFURLCreateStringByAddingPercentEscapes() function provides more options and is more suitable for complex texts.
</p><p>Similarly, if you want to remove the URL encoding from a string, for example in the application:handleOpenURL: UIApplicationDelegate method, use this code:
</p>
<pre>- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url 
{
    NSString *query = [url query];
    CFStringRef clean = CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault, 
                                                                   (CFStringRef)query, 
                                                                   CFSTR(""));

    [controller doSomething:(NSString *)clean];
    CFRelease(clean);
}
</pre>
<a name="Apple_iPhone_Applications" id="Apple_iPhone_Applications"></a><h2> <span class="mw-headline"> Apple iPhone Applications </span></h2>
<a name="Safari" id="Safari"></a><h3> <span class="mw-headline"> Safari </span></h3>
<p>Any URL starting with http:// which does not point to maps.google.com or www.youtube.com is sent to Safari:
</p>
<pre>NSString *stringURL = @"http://wiki.akosma.com/";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><a href="http://applookup.com/2009/01/iphone-apps-with-special-url-shortcuts/" class="external text" title="http://applookup.com/2009/01/iphone-apps-with-special-url-shortcuts/" rel="nofollow">Apparently</a> feed:// opens <a href="http://reader.mac.com" class="external free" title="http://reader.mac.com" rel="nofollow">http://reader.mac.com</a> in Safari.
</p>
<a name="Maps" id="Maps"></a><h3> <span class="mw-headline"> Maps </span></h3>
<p>URLs starting with <a href="http://maps.google.com" class="external free" title="http://maps.google.com" rel="nofollow">http://maps.google.com</a> open up the "Maps" application automatically:
</p>
<pre>NSString *title = @"title";
float latitude = 35.4634;
float longitude = 9.43425;
int zoom = 13;
NSString *stringURL = [NSString stringWithFormat:@"http://maps.google.com/maps?q=%@@%1.6f,%1.6f&amp;z=%d", title, latitude, longitude, zoom];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><a href="http://applookup.com/2009/01/iphone-apps-with-special-url-shortcuts/" class="external text" title="http://applookup.com/2009/01/iphone-apps-with-special-url-shortcuts/" rel="nofollow">It seems</a> that maps:// also opens up the Maps application.
</p>
<a name="Phone" id="Phone"></a><h3> <span class="mw-headline"> Phone </span></h3>
<p>The phone links start with "tel:" but must not contain spaces or brackets (it can contain dashes and "+" signs, though):
</p>
<pre>NSMutableString *phone = [[@"+ 12 34 567 89 01" mutableCopy] autorelease];
[phone replaceOccurrencesOfString:@" " 
                       withString:@"" 
                          options:NSLiteralSearch 
                            range:NSMakeRange(0, [phone length])];
[phone replaceOccurrencesOfString:@"(" 
                       withString:@"" 
                          options:NSLiteralSearch 
                            range:NSMakeRange(0, [phone length])];
[phone replaceOccurrencesOfString:@")" 
                       withString:@"" 
                          options:NSLiteralSearch 
                            range:NSMakeRange(0, [phone length])];
NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phone]];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="SMS" id="SMS"></a><h3> <span class="mw-headline"> SMS </span></h3>
<p>To open the SMS application, just use the sms: protocol in your URL:
</p>
<pre>NSString *stringURL = @"sms:";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>You can specify a phone number, but apparently not a default text for your message:
</p>
<pre>NSString *stringURL = @"sms:+12345678901";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>The same restrictions apply as for phone URLs, regarding spaces, brackets and dashes.
</p>
<a name="Mail" id="Mail"></a><h3> <span class="mw-headline"> Mail </span></h3>
<p>These URLs launch Mail and open the compose message controller:
</p>
<pre>NSString *stringURL = @"mailto:test@example.com";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>You can also provide more information, for a customized subject and body texts:
</p>
<pre>NSString *subject = @"Message subject";
NSString *body = @"Message body";
NSString *address = @"test1@akosma.com";
NSString *cc = @"test2@akosma.com";
NSString *path = [NSString stringWithFormat:@"mailto:%@?cc=%@&amp;subject=%@&amp;body=%@", address, cc, subject, body];
NSURL *url = [NSURL URLWithString:[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>You might also omit some information:
</p>
<pre>NSString *subject = @"Message subject";
NSString *path = [NSString stringWithFormat:@"mailto:?subject=%@", subject];
NSURL *url = [NSURL URLWithString:[path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>For more complex body texts, you might want to use the CFURLCreateStringByAddingPercentEscapes() function, as explained above in this page.
</p>
<a name="Music_.2F_iPod" id="Music_.2F_iPod"></a><h3> <span class="mw-headline"> Music / iPod </span></h3>
<p>The "music:" URL scheme opens the iPod application:
</p>
<pre>NSString *stringURL = @"music:";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Thanks to Eric Dolecki for the information!
</p>
<a name="YouTube" id="YouTube"></a><h3> <span class="mw-headline"> YouTube </span></h3>
<p>URLs starting with <a href="http://www.youtube.com" class="external free" title="http://www.youtube.com" rel="nofollow">http://www.youtube.com</a> open up the "YouTube" application automatically:
</p>
<pre>NSString *stringURL = @"http://www.youtube.com/watch?v=WZH30T99MaM";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>It also works with this URL (which normally brings the Flash video player used by YouTube):
</p>
<pre>NSString *stringURL = @"http://www.youtube.com/v/WZH30T99MaM";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Videos" id="Videos"></a><h3> <span class="mw-headline"> Videos </span></h3>
<p>To open the Videos application, use the "videos:" URL:
</p>
<pre>NSString *stringURL = @"videos:";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="iTunes" id="iTunes"></a><h3> <span class="mw-headline"> iTunes </span></h3>
<p>To open up iTunes, use this URL:
</p>
<pre>NSString *stringURL = @"http://phobos.apple.com/WebObjects/MZStore.woa/wa/viewAlbum?i=156093464&amp;id=156093462&amp;s=143441";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="App_Store" id="App_Store"></a><h3> <span class="mw-headline"> App Store </span></h3>
<p>Very similar to the iTunes URLs:
</p>
<pre>NSString *stringURL = @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=294409923&amp;mt=8";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="iBooks" id="iBooks"></a><h3> <span class="mw-headline"> iBooks </span></h3>
<p>Source: <a href="http://akos.ma/aqdr" class="external free" title="http://akos.ma/aqdr" rel="nofollow">http://akos.ma/aqdr</a> with some help from <a href="https://twitter.com/#!/StuFFmc/status/203209999151931392" class="external text" title="https://twitter.com/#!/StuFFmc/status/203209999151931392" rel="nofollow">StuFFmc</a> to actually get the URL working&nbsp;:)
</p><p>To open the library:
</p>
<pre>NSString *stringURL = @"ibooks://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>To open the iBooks store at a particular book page, you have to add the complete URL to that particular book:
</p>
<pre>NSString *stringURL = @"itms-books://itunes.apple.com/de/book/marchen/id436945766";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<pre>NSString *stringURL = @"itms-bookss://itunes.apple.com/de/book/marchen/id436945766";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Third_Party_Applications" id="Third_Party_Applications"></a><h2> <span class="mw-headline"> Third Party Applications </span></h2>
<a name="AirSharing" id="AirSharing"></a><h3> <span class="mw-headline"> AirSharing </span></h3>
<p>Launch the application:
</p>
<pre>NSString *stringURL = @"airsharing://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Alocola" id="Alocola"></a><h3> <span class="mw-headline"> Alocola </span></h3>
<p>Launch the application:
</p>
<pre>NSString *stringURL = @"alocola://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Ambiance" id="Ambiance"></a><h3> <span class="mw-headline"> Ambiance </span></h3>
<p>For <a href="http://ambianceapp.com/" class="external text" title="http://ambianceapp.com/" rel="nofollow">Ambiance</a> by <a href="http://urbanapps.com/" class="external text" title="http://urbanapps.com/" rel="nofollow">Urban Apps</a>, referred by Matt:
</p><p>Scheme: <tt>amb://</tt>
</p>
<pre>NSString *stringURL = @"amb://home";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Paths (each is self explanatory)
</p>
<ul><li> <tt>home</tt>
</li><li> <tt>player</tt>
</li><li> <tt>featured</tt>
</li><li> <tt>store</tt>
</li><li> <tt>library</tt>
</li><li> <tt>downloads</tt>
</li><li> <tt>favorites</tt>
</li><li> <tt>alarm</tt>
</li><li> <tt>settings</tt>
</li><li> <tt>quickstart</tt>
</li><li> <tt>share</tt>
</li><li> <tt>help</tt>
</li><li> <tt>backup</tt>
</li><li> <tt>recorder</tt>
</li></ul>
<a name="Appigo_Notebook" id="Appigo_Notebook"></a><h3> <span class="mw-headline"> Appigo Notebook </span></h3>
<p>Launch the application:
</p>
<pre>NSString *stringURL = @"appigonotebook://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Appigo_Todo" id="Appigo_Todo"></a><h3> <span class="mw-headline"> Appigo Todo </span></h3>
<p>Launch the application:
</p>
<pre>NSString *stringURL = @"appigotodo://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Create a new task:
</p>
<pre>NSString *template = @"appigotodo://com.example.xyzapp/import?name=%@&amp;note=%@&amp;due-date=%@&amp;priority=%@&amp;repeat=%@";
NSString *name = @"Buy%20some%20milk";
NSString *note = @"Stop%20on%20the%20way%20home%20from%20work.";
NSString *dueDate = @"2009-07-16";
NSString *priority = @"1";
NSString *repeat = @"101";
NSString *stringURL = [NSString stringWithFormat:template, name, note, dueDate, priority, repeat];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Bookmarkers_.2F_Bookmarkers_Pro" id="Bookmarkers_.2F_Bookmarkers_Pro"></a><h3> <span class="mw-headline"> Bookmarkers / Bookmarkers Pro </span></h3>
<pre>NSString *stringURL = @"Bookmarkers://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open Bookmarkers Pro:
</p><p>BookmarkersPro://{url}?jumpback={your url scheme}
</p><p>Open Bookmarkers:
</p><p>Bookmarkers://{url}?jumpback={your url scheme}
</p>
<a name="Call_Recorder_and_Cheap_Calls" id="Call_Recorder_and_Cheap_Calls"></a><h3> <span class="mw-headline"> Call Recorder and Cheap Calls </span></h3>
<p>Call Recorder - IntCall
With the app you can record our outgoing phone calls.
</p><p><a href="https://itunes.apple.com/us/app/call-recorder-intcall/id521680097?mt=8" class="external free" title="https://itunes.apple.com/us/app/call-recorder-intcall/id521680097?mt=8" rel="nofollow">https://itunes.apple.com/us/app/call-recorder-intcall/id521680097?mt=8</a>
</p><p>URL Scheme:
</p><p>callrec://[phone number in international format without leading 0]
</p><p>For example, to call a number in USA use the following scheme:
callrec://12125867000
</p><p>Cheap Calls - IntCall
</p><p>With the app you can make cheap international calls
<a href="https://itunes.apple.com/us/app/cheap-calls-intcall/id502451894?mt=8" class="external free" title="https://itunes.apple.com/us/app/cheap-calls-intcall/id502451894?mt=8" rel="nofollow">https://itunes.apple.com/us/app/cheap-calls-intcall/id502451894?mt=8</a>
</p><p>URL Scheme:
cheapcalls://[phone number in international format without leading 0]
</p><p>For example, to call a number in USA use the following scheme:
cheapcalls://12125867000
</p>
<a name="Duo" id="Duo"></a><h3> <span class="mw-headline"> Duo </span></h3>
<p>Launch the application and pre-populate the update field with your desired Tweet/Facebook update.  Optional:  Provide Duo with the custom URL and return path to your app to provide the user with a button to return to your app.
</p>
<pre>NSString *appID = @"Your Apps Name";
NSString *updateInfo = @"The Tweet/Status Update";
NSString *returnScheme = @"Your Apps Return URL Scheme";
NSString *returnPath = @"Your/Apps/Return/Path";
NSString *baseURL = @"duoxx://updateFaceTwit?";
	
NSString *encodedUpdateInfo = [updateInfo stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
NSString *urlString = [NSString stringWithFormat:@"%@BLappID=%@;BLupdateInfo=%@;BLreturnScheme=%@;BLreturnPath=%@", 
						   baseURL, appID, encodedUpdateInfo, returnScheme, returnPath];

NSURL *url = [NSURL URLWithString:urlString];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><br>
</p>
<a name="The_Cartographer" id="The_Cartographer"></a><h3> <span class="mw-headline"> The Cartographer </span></h3>
<p>Launch app and add a placemark at given coordinate:
</p>
<pre>NSString *title = @"Placemark title (optional)";
NSString *summary = @"Placemark description (optional)";
CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(123.0, 12.0);

title = [(id)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)title, NULL, (CFStringRef)@";/?:@&amp;=+$,", kCFStringEncodingUTF8) autorelease];
summary = [(id)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)summary, NULL, (CFStringRef)@";/?:@&amp;=+$,", kCFStringEncodingUTF8) autorelease];

NSString *cartographerURL = [NSString stringWithFormat:@"cartographer://placemark?coordinate=%lf,%lf&amp;title=%@&amp;summary=%@", 
         coordinate.latitude, coordinate.longitude, title, summary];

NSURL *url = [NSURL URLWithString:cartographerURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Launch app and load either a KML source or a Google My Map:
</p>
<pre>NSString *sourceURL = @"http://....";
// e.g. http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;msa=0&amp;msid=208320711678395936256.00046bbcfdcd1f3ebf64b&amp;z=5

// Optional:
MKCoordinateRegion region = ...;
sourceURL = [sourceURL stringByAppendingFormat:@"&amp;ll=%lf,%lf&amp;spn=%lf,%lf", 
               region.center.latitude, region.center.longitude, region.span.latitudeDelta, region.span.longitudeDelta];

NSURL *url = [NSURL URLWithString:[sourceURL stringByReplacingOccurrencesOfString:@"http://" withString:@"cartographer://"]];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><br>
</p>
<a name="ChatCo" id="ChatCo"></a><h3> <span class="mw-headline"> ChatCo </span></h3>
<pre>NSString *stringURL = @"irc://irc.example.domain/roomName";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<pre>NSString *stringURL = @"irc://irc.example.domain/Nickname@roomName";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<pre>NSString *stringURL = @"irc://irc.example.domain/Nickname!Realname@roomName";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><br>
</p>
<a name="Cobi_Tools" id="Cobi_Tools"></a><h3> <span class="mw-headline"> Cobi Tools </span></h3>
<p>To email device info:
</p>
<pre>[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"cobitools://emailInfo?email=john@appleseed.com"]];
</pre>
<p>To email console logs:
</p>
<pre>[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"cobitools://emailLog?email=john@appleseed.com&amp;filter=textFilter&amp;appname=AppName"]];
</pre>
<a name="Credit_Card_Terminal" id="Credit_Card_Terminal"></a><h3> <span class="mw-headline"> Credit Card Terminal </span></h3>
<p>Launch the application:
</p>
<pre>NSString *stringURL = @"com-innerfence-ccterminal://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="DailyMotion" id="DailyMotion"></a><h3> <span class="mw-headline"> DailyMotion </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"dailymotion://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>To open a video using its ID:
</p>
<pre>NSString *stringURL = [NSString stringWithFormat:@"dailymotion://video/%@", video_id];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><br>
</p>
<a name="Darkslide_.28formerly_Exposure.29" id="Darkslide_.28formerly_Exposure.29"></a><h3> <span class="mw-headline"> Darkslide (formerly Exposure) </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"exposure://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Echofon_.2F_Echofon_Pro_.28formerly_Twitterfon_.2F_Twitterfon_Pro.29" id="Echofon_.2F_Echofon_Pro_.28formerly_Twitterfon_.2F_Twitterfon_Pro.29"></a><h3> <span class="mw-headline"> Echofon / Echofon Pro (formerly Twitterfon / Twitterfon Pro) </span></h3>
<p><a href="http://twitterfon.net/how-to-use.html" class="external text" title="http://twitterfon.net/how-to-use.html" rel="nofollow">(Source)</a>
</p>
<pre>NSString *message = @"http://test.com/";
NSString *stringURL = [NSString stringWithFormat:@"twitterfon:///post?%@", message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<pre>NSString *message = @"http://test.com/";
NSString *stringURL = [NSString stringWithFormat:@"twitterfonpro:///post/username?%@", message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>The "post" parameter in the URL only works for simple URLs; if you want to post URL-encoded text, try the following:
</p>
<pre>NSString *message = @"hello%20from%20TwitterFon!";
NSString *stringURL = [NSString stringWithFormat:@"twitterfonpro:///message?%@", message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Eureka" id="Eureka"></a><h3> <span class="mw-headline"> Eureka </span></h3>
<p>Substitute eureka for http e.g. eureka://en.wikipedia.org/wiki/Apple
</p>
<pre>NSString *page = @"en.wikipedia.org/wiki/Apple";
NSString *stringURL = [NSString stringWithFormat:@"eureka://%@", page];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Explorimmo_immobilier" id="Explorimmo_immobilier"></a><h3> <span class="mw-headline"> Explorimmo immobilier </span></h3>
<pre>NSString *stringURL = @"explorimmo://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Facebook" id="Facebook"></a><h3> <span class="mw-headline"> Facebook </span></h3>
<p>This information comes from <a href="http://iphonedevtools.com/?p=302" class="external text" title="http://iphonedevtools.com/?p=302" rel="nofollow">the iPhoneDevTools website</a>.
</p>
<pre>NSURL *url = [NSURL URLWithString:@"fb://&lt;insert function here&gt;"];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>You can use these options:
</p>
<ul><li> fb://profile – Open Facebook app to the user’s profile
</li><li> fb://friends – Open Facebook app to the friends list
</li><li> fb://notifications – Open Facebook app to the notifications list (NOTE:  there appears to be a bug with this URL.  The Notifications page opens.  However, it’s not possible to navigate to anywhere else in the Facebook app)
</li><li> fb://feed – Open Facebook app to the News Feed
</li><li> fb://events – Open Facebook app to the Events page
</li><li> fb://requests – Open Facebook app to the Requests list
</li><li> fb://notes- Open Facebook app to the Notes page
</li><li> fb://albums – - Open Facebook app to Photo Albums list
</li></ul>
<p>This "works" but, then again, doesn't:
</p>
<ul><li> fb://post/&lt;post_id&gt;
</li></ul>
<p>It only works if the Facebook app has previously loaded the particular post, otherwise it draws a blank.
</p><p><a href="http://stackoverflow.com/questions/5707722/anyone-with-experience-working-with-iphone-facebook/5707825#5707825" class="external text" title="http://stackoverflow.com/questions/5707722/anyone-with-experience-working-with-iphone-facebook/5707825#5707825" rel="nofollow">Apparently</a> there are even more options available (thanks to Alex Salom for the tip!):
</p>
<ul><li> fb://album/%@
</li><li> fb://album/(aid)
</li><li> fb://album/(aid)/cover
</li><li> fb://album/(initWithAID:)
</li><li> fb://album/(initWithAID:)/cover
</li><li> fb://album/new
</li><li> fb://albums
</li><li> fb://birthdays
</li><li> fb://birthdays/(initWithMonth:)/(year:)
</li><li> fb://birthdays/(month)/(year)
</li><li> fb://chat/(fbid)
</li><li> fb://chat/(initWithUID:)
</li><li> fb://chat/(user.fbid)
</li><li> fb://contactimporter
</li><li> fb://contactimporter/invites
</li><li> fb://contactimporter/legalese
</li><li> fb://contactimporter/modal
</li><li> fb://event/%@
</li><li> fb://event/%llu
</li><li> fb://event/(event.fbid)/members/(rsvpStatus)
</li><li> fb://event/(fbid)
</li><li> fb://event/(fbid)/members/attending
</li><li> fb://event/(fbid)/members/declined
</li><li> fb://event/(fbid)/members/not_replied
</li><li> fb://event/(fbid)/members/unsure
</li><li> fb://event/(fbid)/rsvp
</li><li> fb://event/(initWithEventId:)
</li><li> fb://event/(initWithEventId:)/members/(rsvpStatus:)
</li><li> fb://event/(initWithEventId:)/rsvp
</li><li> fb://events
</li><li> fb://events/
</li><li> fb://faceweb/(initWithURL:)
</li><li> fb://facewebmodal/(initWithURL:)
</li><li> fb://feed
</li><li> fb://feed/%@
</li><li> fb://feed/(filter.filterKey)
</li><li> fb://feed/(initWithFilterKey:)
</li><li> fb://feedfilters
</li><li> fb://findfriends
</li><li> fb://findfriends/legalese
</li><li> fb://findfriends/modal
</li><li> fb://friends
</li><li> fb://friends/picker
</li><li> fb://friends/sync
</li><li> fb://friends/sync/(removeData:)
</li><li> fb://friends/sync/disconnect
</li><li> fb://friends/sync/legalese
</li><li> fb://group/(fbid)/members
</li><li> fb://group/(initWithGroupId:)/members
</li><li> fb://groups
</li><li> fb://launcher
</li><li> fb://mailbox
</li><li> fb://mailbox/(folder)
</li><li> fb://mailbox/(initWithFolder:)
</li><li> fb://mailbox/(initWithFolder:)/(tid:)
</li><li> fb://mailbox/(mailbox.folder)/(tid)
</li><li> fb://mailbox/compose
</li><li> fb://mailbox/compose/(fbid)
</li><li> fb://mailbox/compose/(initWithUID:)
</li><li> fb://map
</li><li> fb://messaging
</li><li> fb://messaging/(folder)
</li><li> fb://messaging/(initWithFolder:)
</li><li> fb://messaging/(initWithFolder:)/(tid:)/participants
</li><li> fb://messaging/(initWithFolder:)/thread?tid=(tid:)
</li><li> fb://messaging/(mailbox.folder)/(urlEscapedTid)/participants
</li><li> fb://messaging/(mailbox.folder)/thread?tid=(urlEscapedTid)
</li><li> fb://messaging/compose
</li><li> fb://messaging/compose/(fbid)
</li><li> fb://messaging/compose/(initWithUID:)
</li><li> fb://messaging/original_message?mid=(commentId)
</li><li> fb://messaging/original_message?mid=(initWithMessageId:)
</li><li> fb://nearby
</li><li> fb://note/%@
</li><li> fb://note/(initWithNoteId:)
</li><li> fb://note/(initWithNoteId:)/edit
</li><li> fb://note/(noteId)
</li><li> fb://note/(noteId)/edit
</li><li> fb://note/compose
</li><li> fb://notes
</li><li> fb://notifications
</li><li> fb://online
</li><li> fb://online#offline
</li><li> fb://online#online
</li><li> fb://pages
</li><li> fb://photo/%@/0/%@
</li><li> fb://photo/(album.user.fbid)/(album.aid)/(pid)
</li><li> fb://photo/(album.user.fbid)/(album.aid)/(pid)/feedback
</li><li> fb://photo/(fbid)/profilepic
</li><li> fb://photo/(initWithProfilePicturesUID:)/profilepic
</li><li> fb://photo/(initWithUID:)/(aid:)/(pid:)
</li><li> fb://photo/(initWithUID:)/(aid:)/(pid:)/feedback
</li><li> fb://photosapp
</li><li> fb://place/%@
</li><li> fb://place/(initWithPageId:)
</li><li> fb://place/(targetId)
</li><li> fb://place/addfriends
</li><li> fb://place/addphoto
</li><li> fb://place/create
</li><li> fb://places
</li><li> fb://places/%lld/%lld
</li><li> fb://places/(initWithCheckinAtPlace:)/(byUser:)
</li><li> fb://places/legalese/tagged/%lld/%lld
</li><li> fb://places/legalese/tagged/(initWithTaggedAtPlace:)/(byUser:)
</li><li> fb://post/%@
</li><li> fb://post/%@_%@
</li><li> fb://post/(initWithPostId:)
</li><li> fb://post/(initWithPostId:)/tagged
</li><li> fb://post/(postId)
</li><li> fb://post/(postId)/tagged
</li><li> fb://post/(postId)/untagSelf
</li><li> fb://post/(untagSelfFromPostWithId:)/untagSelf
</li><li> fb://profile
</li><li> fb://profile/
</li><li> fb://profile/%@
</li><li> fb://profile/%lld
</li><li> fb://profile/(addFan:)/addfan
</li><li> fb://profile/(fbid)
</li><li> fb://profile/(fbid)/addfan
</li><li> fb://profile/(fbid)/addfriend
</li><li> fb://profile/(fbid)/fanpages
</li><li> fb://profile/(fbid)/fans
</li><li> fb://profile/(fbid)/favorite
</li><li> fb://profile/(fbid)/friends
</li><li> fb://profile/(fbid)/info
</li><li> fb://profile/(fbid)/menu
</li><li> fb://profile/(fbid)/mutualfriends
</li><li> fb://profile/(fbid)/photos
</li><li> fb://profile/(fbid)/poke
</li><li> fb://profile/(fbid)/removefriend
</li><li> fb://profile/(fbid)/wall
</li><li> fb://profile/(initWithFBID:)/menu
</li><li> fb://profile/(initWithFansUID:)/fans
</li><li> fb://profile/(initWithFriendsUID:)/friends
</li><li> fb://profile/(initWithInfoUID:)/info
</li><li> fb://profile/(initWithMutualFriendsUID:)/mutualfriends
</li><li> fb://profile/(initWithPhotosUID:)/photos
</li><li> fb://profile/(initWithUID:)
</li><li> fb://profile/(initWithUID:)/addfriend
</li><li> fb://profile/(initWithUID:)/fanpages
</li><li> fb://profile/(initWithUID:)/poke
</li><li> fb://profile/(initWithUID:)/removefriend
</li><li> fb://profile/(initWithWallUID:)/wall
</li><li> fb://profile/(toggleFavorite:)/favorite
</li><li> fb://profile/(user.fbid)/fans
</li><li> fb://profile/(user.fbid)/friends
</li><li> fb://profile/(user.fbid)/mutualfriends
</li><li> fb://profile/0
</li><li> fb://publish
</li><li> fb://publish/mailbox/(initWithFolder:)/(tid:)
</li><li> fb://publish/mailbox/(mailbox.folder)/(tid)
</li><li> fb://publish/photo/(album.user.fbid)/(album.aid)/(pid)
</li><li> fb://publish/photo/(initWithUID:)/(aid:)/(pid:)
</li><li> fb://publish/post/(initWithPostId:)
</li><li> fb://publish/post/(postId)
</li><li> fb://publish/profile/(fbid)
</li><li> fb://publish/profile/(initWithUID:)
</li><li> fb://publish/profile/(owner.fbid)
</li><li> fb://requests
</li><li> fb://root
</li><li> fb://upload
</li><li> fb://upload/%@/album/%lld/%@
</li><li> fb://upload/%@/checkin/%lld
</li><li> fb://upload/%@/profile/%lld
</li><li> fb://upload/(initWithSource:)/album/(uid:)/(aid:)
</li><li> fb://upload/(initWithSource:)/checkin/(checkinId:)
</li><li> fb://upload/(initWithSource:)/profile/(uid:)
</li><li> fb://upload/actions
</li><li> fb://upload/actions/album/(initWithUID:)/(aid:)
</li><li> fb://upload/actions/album/(user.fbid)/(aid)
</li><li> fb://upload/actions/checkin/(checkinId)/
</li><li> fb://upload/actions/checkin/(initWithCheckinId:)
</li><li> fb://upload/actions/newalbum
</li><li> fb://upload/actions/profile/(fbid)
</li><li> fb://upload/actions/profile/(initWithUID:)
</li><li> fb://upload/actions/resume
</li><li> fb://upload/album/(showUploadMenuWithUID:)/(aid:)
</li><li> fb://upload/album/(user.fbid)/(aid)
</li><li> fb://upload/checkin/(checkinId)
</li><li> fb://upload/checkin/(showUploadMenuWithCheckinID:)
</li><li> fb://upload/discard
</li><li> fb://upload/profile/(fbid)
</li><li> fb://upload/profile/(owner.fbid)
</li><li> fb://upload/profile/(showUploadMenuWithUID:)
</li><li> fb://upload/resume
</li><li> fb://userset
</li><li> fb://video/%@
</li><li> fb://video/(playVideoWithId:)
</li><li> fb://video/(videoId)
</li></ul>
<p>Says Sam Cornwell:
</p><p>As of the most recent facebook update 4.0 for iPhone and iPad, 
</p>
<ul><li> fb://page/(fbid) will work to open to a specific page.
</li><li> and fb://place/(fbid) will take you to a location page
</li></ul>
<a name="Geocaching_Buddy_.28_GCBuddy_.29" id="Geocaching_Buddy_.28_GCBuddy_.29"></a><h3> <span class="mw-headline"> Geocaching Buddy ( GCBuddy ) </span></h3>
<p>URL scheme is gcbuddy:// so to launch the application:
</p>
<pre>NSString *stringURL = @"gcbuddy://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>To add a cache to gcbuddy (simple example):
</p>
<pre>NSString *stringURL = @"gcbuddy://add/cache?id=GC12345&amp;name=A%20test%20cache";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>See the <a href="http://www.btstsoft.nl/gcbuddy/api.html" class="external text" title="http://www.btstsoft.nl/gcbuddy/api.html" rel="nofollow">full documentation</a> for all optional parameters.
</p>
<a name="Geopher_Lite" id="Geopher_Lite"></a><h3> <span class="mw-headline"> Geopher Lite </span></h3>
<pre>NSString *lat = @"12.345";
NSString *lon = @"6.789";
NSString *waypoint = @"GC12345";
NSString *stringURL = [NSString stringWithFormat:@"geopherlite://setTarget;lat=%@;lon=%@;waypoint=%@", lat, lon, waypoint];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Google_Chrome" id="Google_Chrome"></a><h3> <span class="mw-headline"> Google Chrome </span></h3>
<p>Source: <a href="https://developers.google.com/chrome/mobile/docs/ios-links" class="external free" title="https://developers.google.com/chrome/mobile/docs/ios-links" rel="nofollow">https://developers.google.com/chrome/mobile/docs/ios-links</a>
</p><p>To check if Chrome is installed, an app can simply check if either of these URI schemes is available:
</p>
<pre>[[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"googlechrome://"]];
</pre>
<p>This step is useful in case an app would like to change the UI depending on if Chrome is installed or not. For instance the app could add an option to open URLs in Chrome in a share menu or action sheet.
</p><p>To actually open a URL in Chrome, the URI scheme provided in the URL must be changed from http or https to the Google Chrome equivalent. The following sample code opens a URL in Chrome:
</p>
<pre>NSURL *inputURL = &lt;the URL to open&gt;;
NSString *scheme = inputURL.scheme;

// Replace the URL Scheme with the Chrome equivalent.
NSString *chromeScheme = nil;
if ([scheme isEqualToString:@"http"]) {
  chromeScheme = @"googlechrome";
} else if ([scheme isEqualToString:@"https"]) {
  chromeScheme = @"googlechromes";
}

// Proceed only if a valid Google Chrome URI Scheme is available.
if (chromeScheme) {
  NSString *absoluteString = [inputURL absoluteString];
  NSRange rangeForScheme = [absoluteString rangeOfString:@":"];
  NSString *urlNoScheme =
      [absoluteString substringFromIndex:rangeForScheme.location];
  NSString *chromeURLString =
      [chromeScheme stringByAppendingString:urlNoScheme];
  NSURL *chromeURL = [NSURL URLWithString:chromeURLString];

  // Open the URL with Chrome.
  [[UIApplication sharedApplication] openURL:chromeURL];
}
</pre>
<p>If Chrome is installed, the above code converts the URI scheme found in the URL to the Google Chrome equivalent. When Google Chrome opens, the URL passed as a parameter will be opened in a new tab.
</p>
<a name="Google_Earth" id="Google_Earth"></a><h3> <span class="mw-headline"> Google Earth </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"comgoogleearth://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Image_Data" id="Image_Data"></a><h3> <span class="mw-headline"> Image Data </span></h3>
<p>Image Data is a universal app that displays the metadata stored inside an image.
<a href="http://itunes.apple.com/us/app/image-metadata-viewer/id412509237?mt=8" class="external free" title="http://itunes.apple.com/us/app/image-metadata-viewer/id412509237?mt=8" rel="nofollow">http://itunes.apple.com/us/app/image-metadata-viewer/id412509237?mt=8</a>
</p><p>Image Data can be opened by other application by using the ALAssets representation URL where assets-library is replaced by imagedata. For example:
</p>
<pre>imagedata://asset/asset.JPG?id=E9141C24-A5C9-47E2-A2AE-8580A21CD612&amp;ext=JPG
</pre>
<pre>ALAsset *asset = //reference to a ALAsset
ALAssetRepresentation *representation = [asset defaultRepresentation];
NSURL *assetURL = [representation url];
NSString *assetURLAsString = [assetURL absoluteString];

NSURL *imageDataURL = [NSURL URLWithString:[assetURLAsString stringByReplacingOccurrencesOfString:@"assets-library" withString:@"imagedata"]];

[[UIApplication sharedApplication] openURL:imageDataURL];
</pre>
<a name="IMDb_Movies_.26_TV" id="IMDb_Movies_.26_TV"></a><h3> <span class="mw-headline"> IMDb Movies &amp; TV </span></h3>
<p>The IMDb 2.0 app now has three URL schemes:
</p>
<pre>// with a search keyword:
imdb:///find?q=godfather

// with a title id:
imdb:///title/tt0068646/

// with a name id:
imdb:///name/nm0000199/
</pre>
<p>More URLs supported in version 2.1
</p>
<pre>imdb:///showtimes
imdb:///title/tt0068646/cinemashowtimes
imdb:///boxoffice
imdb:///chart/moviemeter
imdb:///chart/starmeter
imdb:///chart/top
imdb:///chart/bottom
imdb:///chart/tv
imdb:///feature/comingsoon
imdb:///feature/bestpicture
imdb:///feature/tvrecaps
imdb:///feature/borntoday
</pre>
<p>Alternatively, the app recognizes version specific URL schemes of the form imdb-XXYY where XX is the app major version and YY is the app minor version. The 2.1 app was the first to support this scheme. This can also be used as a check from external apps to detect whether a required minimum version of the app is installed. For example, all IMDb app versions from 2.1 and higher will recognize the imdb-0201 scheme.
</p>
<a name="INRIX_Traffic" id="INRIX_Traffic"></a><h3> <span class="mw-headline"> INRIX Traffic </span></h3>
<p>Launch the application (as of version 3.1):
</p>
<pre>NSString *stringURL = @"inrixtraffic://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="iTranslate_.7E_the_universal_translator" id="iTranslate_.7E_the_universal_translator"></a><h3> <span class="mw-headline"> iTranslate ~ the universal translator </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"itranslate://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>To translate a text:
</p>
<pre>NSString *textToTranslate = @"Hello world"; // Text must be URL-encoded...!
textToTranslate = [textToTranslate stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
NSString *test =  [NSString	stringWithFormat:@"itranslate://translate?from=en&amp;to=de&amp;text=%@",textToTranslate];
NSURL *url = [[NSURL alloc] initWithString:test];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>You can find a list of all supported language codes here: <a href="http://code.google.com/apis/language/translate/v2/using_rest.html#language-params" class="external free" title="http://code.google.com/apis/language/translate/v2/using_rest.html#language-params" rel="nofollow">http://code.google.com/apis/language/translate/v2/using_rest.html#language-params</a>
</p>
<a name="Junos_Pulse_VPN_app_for_iOS" id="Junos_Pulse_VPN_app_for_iOS"></a><h3> <span class="mw-headline"> Junos Pulse VPN app for iOS </span></h3>
<pre>NSString *stringURL = @"junospulse://&lt;server-host&gt;/&lt;server-path&gt;?method={vpn}&amp;action={start|stop}";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Loan_Plan_-_Amortization_Calculator" id="Loan_Plan_-_Amortization_Calculator"></a><h3> <span class="mw-headline"> Loan Plan - Amortization Calculator </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"loanplan://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Navigon" id="Navigon"></a><h3> <span class="mw-headline"> Navigon </span></h3>
<p>Example of a URL to start navigation to an address:
</p>
<pre>navigon://-|-|CHE|8052|Z=C3=9CRICH|KATZENBACHSTRASSE|51|-|-|***|-|-|-|-|-|-|-|-|-|-|-|-|-|-|-
</pre>
<a name="Notitas" id="Notitas"></a><h3> <span class="mw-headline"> Notitas </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"notitas://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>To create a new note:
</p>
<pre>NSString *note = @"your%20note%20here"; // It must be URL-encoded...!
NSString *stringURL = [NSString stringWithFormat:@"notitas:///new?%@", note];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="OpenTable" id="OpenTable"></a><h3> <span class="mw-headline"> OpenTable </span></h3>
<p>To search for open seats at a restaurant:
</p>
<pre>NSString *restaurantID = @"168"; // Numeric OpenTable Restaurant ID
NSString *partySize = @"3"; // Integer
NSString *stringURL = [NSString stringWithFormat:@"reserve://opentable.com/%@?partySize=%@", restaurantID, partySize];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Opera_Mini" id="Opera_Mini"></a><h3> <span class="mw-headline"> Opera Mini </span></h3>
<p>Following URL schemes are handled by Opera Mini 7.0 and later:
</p>
<ul><li> ohttp:// e.g. ohttp://www.opera.com/
</li><li> ohttps://
</li><li> oftp://
</li></ul>
<pre>NSString *stringURL = @"ohttp://www.opera.com/";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="pic2shop" id="pic2shop"></a><h3> <span class="mw-headline"> pic2shop </span></h3>
<p>See the <a href="http://www.pic2shop.com/developers/developers.html" class="external text" title="http://www.pic2shop.com/developers/developers.html" rel="nofollow">full documentation, code samples, and online demo</a>.
</p><p>Start the barcode scanner, read the barcode, then call back the given URL with the UPC/EAN digits.
The callback URL can be another iPhone custom URL (p2sclient: in the example below) or a web site:
</p>
<pre>NSURL *url = [NSURL URLWithString:@"pic2shop://scan?callback=p2sclient%3A//EAN"]; 
[[UIApplication sharedApplication] openURL:url]
</pre>
<p>Lookup a product by UPC/EAN:
</p>
<pre>NSURL *url = [NSURL URLWithString:@"pic2shop://lookup?ean=9780321503619"]; 
[[UIApplication sharedApplication] openURL:url]
</pre>
<a name="Pinterest" id="Pinterest"></a><h3> <span class="mw-headline"> Pinterest </span></h3>
<p><a href="http://www.sosuke.com/index.php/2012/05/08/adding-pinterest-sharing-to-your-ios-app/" class="external text" title="http://www.sosuke.com/index.php/2012/05/08/adding-pinterest-sharing-to-your-ios-app/" rel="nofollow">Source</a>
</p><p>URL Scheme: pinit12://pinterest.com/pin/create/bookmarklet/?
</p>
<pre>NSURL *url = [NSURL URLWithString:@"pinit12://pinterest.com/pin/create/bookmarklet/?"]; 
[[UIApplication sharedApplication] openURL:url]
</pre>
<p>Parameters:
</p>
<ul><li> media: a direct link to the image or I think an encoded image (base64?)
</li><li> url: the URL of the source website where the image was found
</li><li> description: 500 characters max
</li><li> is_video: self describing
</li></ul>
<a name="Pocketpedia" id="Pocketpedia"></a><h3> <span class="mw-headline"> Pocketpedia </span></h3>
<p>The URL schemes initiates a search of the users media items or begins an online search for a book, movie, CD or game to add to the users library.
</p><p>The schemes are compatible with the Mac version of the software so will work on both iOS and Mac.
</p><p>The search scheme works by targeting the media type via the URL protocol and then add the search keywords (if left blank the search field is shown with text input ready for the user to type). Following are some examples of the four media protocols:
</p>
<pre>dvdpedia://search?Gladiator
bookpedia://search?Tolkien
cdpedia://search?Sting
gamepedia://search?Zelda
</pre>
<p>The second URL option is to filter the current users collections to find a specific item already in the collection. The filter can be done via keyword or target one of the following 4 specific fields: title, director, artist or author via regular URL get encoding.
</p>
<pre>bookpedia://filterCollection?author=Orson%20Scott%20Card
cdpedia://filterCollection?artist=Sting
gamepedia://filterCollection?Nintendo
</pre>
<p><br>
</p>
<a name="Portscan" id="Portscan"></a><h3> <span class="mw-headline"> Portscan </span></h3>
<p>Launch a scan:
</p>
<pre>NSString *ip = @"192.168.1.1";
NSString *stringURL = [NSString stringWithFormat:@"portscan://%@", ip];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>

<a name="PureCalc" id="PureCalc"></a><h3> <span class="mw-headline"> PureCalc </span></h3>
<p>Run PureCalc
</p>
<pre>NSString *stringURL = @purecalc://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Access PureCalc Support Website
</p>
<pre>NSString *stringURL = @purecalc://help";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>View PureCalc Version Number
</p>
<pre>NSString *stringURL = @purecalc://version";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Generate Email to PureCalc Support Team
</p>
<pre>NSString *stringURL = @purecalc://support";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>


<a name="Quran" id="Quran"></a><h3> <span class="mw-headline"> Qur'an Memorizer </span></h3>
<pre>
quran://a/b

Where a and b are positive integers.
This will open the Qur'an Memorizer app at Chapter 'a' verse 'b'.
</pre>




<a name="Round_Tuit" id="Round_Tuit"></a><h3> <span class="mw-headline"> Round Tuit </span></h3>
<p>Add new items to the todo list:
</p><p>roundtuit://add/[todo item 1]/[todo item 2]/[todo item 3]/...
</p>
<pre>NSArray *todos = [NSArray arrayWithObjects:@"todo item 1", @"todo item 2"];
NSString *stringURL = [NSString stringWithFormat:@"roundtuit://add/%@", [todos componentsJoinedByString:@"/"]];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Ships.C2.B2_Viewer" id="Ships.C2.B2_Viewer"></a><h3> <span class="mw-headline"> Ships² Viewer </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"ships2://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Site_Check" id="Site_Check"></a><h3> <span class="mw-headline"> Site Check </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"checkit://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Check current url:
</p>
<pre>NSString *whatever = @"http://whatever.com";
NSString *stringURL = [NSString stringWithFormat:@"checkit://%@", whatever];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Surfboard_.28iPad.29" id="Surfboard_.28iPad.29"></a><h3> <span class="mw-headline"> Surfboard (iPad) </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"surfboard://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Add a bookmark / site into the app:
</p>
<pre>NSString *URL = @"whatever.com";
NSString *title = @"nonEscapedTitle";

//you will want to escape these before sending real titles / URLS

NSString *stringURL = [NSString stringWithFormat:@"surfboard://add?url=%@&amp;title=%@", URL,title];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><a href="http://surfboardapp.com/tips/general/bookmarklet/" class="external text" title="http://surfboardapp.com/tips/general/bookmarklet/" rel="nofollow">Bookmarklet usage of this</a>
</p>
<a name="TimeLog" id="TimeLog"></a><h3> <span class="mw-headline"> TimeLog </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"timelog://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Start a timer for a project
</p>
<pre>NSString * clientName = @"John%20Appleseed";
NSString * projectName = @"Just%20another%20project";
NSString * categoryName = @"Webdesign";
NSString *stringURL = [NSString stringWithFormat:@"timelog://start?client=%@&amp;project=%@&amp;category=%@", clientName,projectName, categoryName];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Timer" id="Timer"></a><h3> <span class="mw-headline"> Timer </span></h3>
<p><a href="http://pacolabs.com/iOS/Timer/" class="external autonumber" title="http://pacolabs.com/iOS/Timer/" rel="nofollow">[1]</a>
</p><p>The URL scheme to open Timer is timer://
One or several actions can be added to this scheme, as the query parameters.
</p><p>Here are the different actions.
They take, as values, a coma separated list of screens (represented by their position in the tab bar of Timer).
</p>
<ul><li> show&nbsp;: shows the specified screen.
</li><li> start&nbsp;: starts the timer(s) in the specified screen(s).
</li><li> stop&nbsp;: stops the timer(s) in the specified screen(s).
</li><li> startstop&nbsp;: toggles the start/stop state of the timer(s) in the specified screen(s).
</li><li> resume&nbsp;: resumes the timer(s) in the specified screen(s).
</li><li> pause&nbsp;: pauses the timer(s) in the specified screen(s).
</li><li> resumepause&nbsp;: toggles the resume/pause state of the timer(s) in the specified screen(s).
</li><li> lock&nbsp;: locks the specified screen(s).
</li><li> unlock&nbsp;: unlocks the specified screen(s).
</li></ul>
<p>The x-success parameter from the x-callback-url protocol can be used to go back to the source app.
</p>
<ul><li> x-success&nbsp;: the (callback) url to open once all the actions are done.
</li></ul>
<p><br>
Samples&nbsp;:
</p>
<pre>timer://&nbsp;: Open Timer
timer://?resumepause=1&nbsp;: Open Timer and pause (or resume) the first timer.
timer://?show=0&nbsp;: Open Timer and show the list screen.
timer://?start=1,2&amp;x-success=http%3A%2F%2Fpacolabs.com%2FiOS%2FTimer%2F%23Help&nbsp;: Open Timer, start timers 1 and 2 and goes to safari.
timer://?stop=0&amp;show=1&amp;start=1&amp;lock=1&nbsp;: Stop all the timers, show timer 1, start it and lock the screen.
</pre>
<a name="TomTom" id="TomTom"></a><h3> <span class="mw-headline"> TomTom </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"tomtomhome://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Tweetbot" id="Tweetbot"></a><h3> <span class="mw-headline"> Tweetbot </span></h3>
<p>Taken from the official <a href="http://tapbots.com/blog/development/tweetbot-url-scheme" class="external text" title="http://tapbots.com/blog/development/tweetbot-url-scheme" rel="nofollow">Tweetbot URL schemes page</a>:
</p><p>Valid as of version 1.0.2
</p>
<pre>tweetbot://&lt;screenname&gt;/timeline
tweetbot://&lt;screenname&gt;/mentions
tweetbot://&lt;screenname&gt;/retweets
tweetbot://&lt;screenname&gt;/direct_messages
tweetbot://&lt;screenname&gt;/lists
tweetbot://&lt;screenname&gt;/favorites
tweetbot://&lt;screenname&gt;/search
tweetbot://&lt;screenname&gt;/search?query=&lt;text&gt;
tweetbot://&lt;screenname&gt;/status/&lt;tweet_id&gt;
tweetbot://&lt;screenname&gt;/user_profile/&lt;profile_screenname&gt;
tweetbot://&lt;screenname&gt;/post
tweetbot://&lt;screenname&gt;/post?text=&lt;text&gt;
</pre>
<p>The &lt;screename&gt; argument is optional. You may include it to go to a specific account or leave it blank and either the current or default user account will be chosen instead. As usual all query &lt;text&gt; should be URL encoded and other then that everything should be pretty obvious.
</p><p>Valid as of version 1.3
</p>
<pre>tweetbot://&lt;screenname&gt;/post?text=&lt;text&gt;&amp;callback_url=&lt;url&gt;&amp;in_reply_to_status_id=&lt;tweet_id&gt;
tweetbot://&lt;screenname&gt;/search?query=&lt;text&gt;&amp;callback_url=&lt;url&gt;
tweetbot://&lt;screenname&gt;/status/&lt;tweet_id&gt;?callback_url=&lt;url&gt;
tweetbot://&lt;screenname&gt;/user_profile/&lt;screenname|user_id&gt;?callback_url=&lt;url&gt;
tweetbot://&lt;screenname&gt;/follow/&lt;screenname|user_id&gt;
tweetbot://&lt;screenname&gt;/unfollow/&lt;screenname|user_id&gt;
tweetbot://&lt;screenname&gt;/favorite/&lt;tweet_id&gt;
tweetbot://&lt;screenname&gt;/unfavorite/&lt;tweet_id&gt;
tweetbot://&lt;screenname&gt;/retweet/&lt;tweet_id&gt;
</pre>
<p>The argument callback_url is an URL encoded URL that will be opened in Safari once the Post view closes.
</p><p>Valid as of version 1.4
</p>
<pre>tweetbot://&lt;screenname&gt;/list/&lt;list_id&gt;?callback_url=&lt;url&gt;
</pre>
<a name="Tweetie" id="Tweetie"></a><h3> <span class="mw-headline"> Tweetie </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"tweetie://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Start a tweet with a shortened URL:
</p>
<pre>NSString *shortened_url = @"http://your.url.com";
NSString *stringURL = [NSString stringWithFormat:@"tweetie://%@", shortened_url];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Send a message:
</p>
<pre>NSString *message = @"your%20message%20here"; // It must be URL-encoded...!
NSString *stringURL = [NSString stringWithFormat:@"tweetie:///post?message=%@", message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><br>
</p>
<a name="Twittelator_Pro" id="Twittelator_Pro"></a><h3> <span class="mw-headline"> Twittelator Pro </span></h3>
<p><a href="http://www.stone.com/iPhone/Twittelator/Twittelator_API.html" class="external text" title="http://www.stone.com/iPhone/Twittelator/Twittelator_API.html" rel="nofollow">The author has really done a great job documenting this URL scheme! Check it out!</a>
</p><p>Launch the application:
</p>
<pre>NSURL* url = [NSURL URLWithString:@"twit://"];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Send a message:
</p>
<pre>NSString *message = @"URL%20encoded%20message%20here";
NSString *stringURL = [NSString stringWithFormat:@"twit:///post?message=%@", message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p><br>
</p>
<a name="Twitter" id="Twitter"></a><h3> <span class="mw-headline"> Twitter </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"twitter://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Parameters: (provided by Luis Fernández, thanks!)
</p>
<pre>twitter://status?id=12345
twitter://user?screen_name=lorenb
twitter://user?id=12345
twitter://status?id=12345
twitter://timeline
twitter://mentions
twitter://messages
twitter://list?screen_name=lorenb&amp;slug=abcd
twitter://post?message=hello%20world
twitter://post?message=hello%20world&amp;in_reply_to_status_id=12345
twitter://search?query=%23hashtag
</pre>
<a name="Twitterriffic_.2F_Twitterriffic_Premium" id="Twitterriffic_.2F_Twitterriffic_Premium"></a><h3> <span class="mw-headline"> Twitterriffic / Twitterriffic Premium </span></h3>
<p>To launch the application:
</p>
<pre>NSString *stringURL = @"twitterrific://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>To  write a tweet:
</p>
<pre>NSString *message = @"hello!";
NSString *stringURL = [NSString stringWithFormat:@"twitterrific:///post?message=%@", message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Unfragment" id="Unfragment"></a><h3> <span class="mw-headline"> Unfragment </span></h3>
<p>(1.4 or above, formerly Shattered)
</p><p>To launch a Flickr photo puzzle with hidden message:
</p>
<pre>NSMutableDictionary* code = [NSMutableDictionary dictionaryWithCapacity:7];

[code setObject:senderNameString forKey:@"name"];
[code setObject:messageString forKey:@"message"];

[code setObject:photoSourceURLString forKey:@"URL"];
[code setObject:photoIdString forKey:@"id"];
[code setObject:photoSecretString forKey:@"secret"];

[code setObject:[NSNumber numberWithInt:numberOfPieces] forKey:@"piece"];
[code setObject:[NSNumber numberWithBool:rotationOnOrOff] forKey:@"rotation"];

NSString* JSONString = [code JSONRepresentation];
NSData* data = [JSONString dataUsingEncoding:NSUTF8StringEncoding];

NSString* base64String = [data base64Encoding]; //modified Base64 for URL

NSString* unfragmentURLString = [NSString stringWithFormat:@"unfragment://?code=%@", base64String];

NSURL *url = [NSURL URLWithString:unfragmentURLString];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Wikiamo" id="Wikiamo"></a><h3> <span class="mw-headline"> Wikiamo </span></h3>
<pre>NSString *stringURL = [NSString stringWithFormat:@"wikiamo://%@", page];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<pre>NSString *stringURL = [NSString stringWithFormat:@"wikipedia://%@", page];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Yummy" id="Yummy"></a><h3> <span class="mw-headline"> Yummy </span></h3>
<pre>NSString *stringURL = [NSString stringWithFormat:@"yummy://post?url=%@&amp;title=%@", encoded_url, encoded_title];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Swiss_Phone_Book_by_local.ch_.28from_version_1.5.29" id="Swiss_Phone_Book_by_local.ch_.28from_version_1.5.29"></a><h3> <span class="mw-headline"> Swiss Phone Book by local.ch (from version 1.5) </span></h3>
<p>URL examples to launch a search for a name or phone number:
</p><p>localch://tel/q?vikram
</p><p>localch://tel/q?0443091040
</p>
<pre>NSString *stringURL = [NSString stringWithFormat:@"localch://tel/q?%@", encoded_query];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Seesmic" id="Seesmic"></a><h3> <span class="mw-headline"> Seesmic </span></h3>
<p>Open app on Spaces (Page 1)
</p>
<pre>NSString *stringURL = [NSString stringWithString:@"x-seesmic://spaces"];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on Main Timeline for the Twitter account “mathieu”
</p>
<pre>NSString *username = @"mathieu";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://twitter_timeline?twitter_screen_name=%@", username];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on Replies for account “mathieu”
</p>
<pre>NSString *username = @"mathieu";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://twitter_replies?twitter_screen_name=%@", username];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on Retweets for account “mathieu”
</p>
<pre>NSString *username = @"mathieu";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://twitter_retweets?twitter_screen_name=%@", username];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on Direct Messages for account “mathieu”
</p>
<pre>NSString *username = @"mathieu";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://twitter_messages?twitter_screen_name=%@", username];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on the Twitter profile view for “mathieu”
</p>
<pre>NSString *username = @"mathieu";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://twitter_profile?twitter_screen_name=%@", username];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on tweet view for Tweet ID “2814526203”
</p>
<pre>NSString *tweet_id = @"2814526203";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://twitter_status?id=%@", tweet_id];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on the Twitter search results for query “Apple iPhone”
</p>
<pre>// Query will be encoded using stringByAddingPercentEscapesUsingEncoding
NSString *search_query = @"Apple%20iPhone";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://twitter_search?query=%@", search_query];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on the Facebook Main Timeline (if the user added a Facebook account, or else the Spaces view will be launched)
</p>
<pre>NSString *stringURL = [NSString stringWithString:@"x-seesmic://facebook_timeline"];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on Composer view for the Twitter account “mathieu” and with the content “this is a tweet” in the text field
</p>
<pre>// Query will be encoded using stringByAddingPercentEscapesUsingEncoding
NSString *message = @"this%20is%20a%20tweet";
NSString *username = @"mathieu";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://update?twitter_screen_name=%@&amp;status=%@", username, message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<pre>// Query will be encoded using stringByAddingPercentEscapesUsingEncoding
NSString *message = @"this%20is%20a%20tweet%20with%20a%20url%3A%20http%3A%2F%2F";
NSString *username = @"mathieu";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://update?twitter_screen_name=%@&amp;status=%@", username, message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open the Composer view without knowing the account and with the content “this is a tweet” in the text field
</p>
<pre>// Query will be encoded using stringByAddingPercentEscapesUsingEncoding
NSString *message = @"this%20is%20a%20tweet";
NSString *stringURL = [NSString stringWithFormat:@"x-seesmic://update?status=%@", message];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Open app on Composer view (no content, no account app will select default account)
</p>
<pre>NSString *stringURL = [NSString stringWithString:@"x-seesmic://update"];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Offline_Pages" id="Offline_Pages"></a><h3> <span class="mw-headline"> Offline Pages </span></h3>
<p>Launch app
</p>
<pre>NSString *stringURL = [NSString stringWithString:@"offlinepages:"];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Launch app and add URL <a href="http://www.apple.com" class="external free" title="http://www.apple.com" rel="nofollow">http://www.apple.com</a> to save queue
</p>
<pre>NSString *httpURL = [NSString stringWithString:@"http://www.apple.com"];
NSString *offlineURL = [NSString stringWithFormat:@"offlinepages:%@", httpURL];
NSURL *url = [NSURL URLWithString:offlineURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Launch app and add URL <a href="http://www.apple.com" class="external free" title="http://www.apple.com" rel="nofollow">http://www.apple.com</a> to save queue with custom title "Apple website"
</p>
<pre>NSString *httpURL = [NSString stringWithString:@"http://www.apple.com"];
NSString *title= [NSString stringWithString:@"Apple website"];
NSString *encodedTitle = [title stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
NSString *offlineURL = [NSString stringWithFormat:@"offlinepages:title=%@&amp;url=%@", encodedTitle, httpURL];
NSURL *url = [NSURL URLWithString:offlineURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Terminology" id="Terminology"></a><h3> <span class="mw-headline"> Terminology </span></h3>
<p>Launch the application:
</p>
<pre>NSString *stringURL = @"terminology://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Lookup a word or phrase in the dictionary:
</p>
<pre>NSString *template = @"terminology://x-callback-url/1.0/lookup?text=%@";
NSString *text = @"fun";
NSString *stringURL = [NSString stringWithFormat:template, text];
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Request a replacement word also available, see <a href="http://agiletortoise.com/developers/terminology" class="external text" title="http://agiletortoise.com/developers/terminology" rel="nofollow">full developer documentation</a>
</p>
<a name="ZenTap" id="ZenTap"></a><h3> <span class="mw-headline"> ZenTap </span></h3>
<p>Launch the application:
</p>
<pre>NSString *stringURL = @"zentap://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Launch the applicaction with text:
</p>
<pre>NSString *stringURL = @"zentap://myTextToShowInZenTap";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="ZenTap_Pro" id="ZenTap_Pro"></a><h3> <span class="mw-headline"> ZenTap Pro </span></h3>
<p>Launch the application:
</p>
<pre>NSString *stringURL = @"zentappro://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Launch the applicaction with text:
</p>
<pre>NSString *stringURL = @"zentappro://myTextToShowInZenTap";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="TextFaster" id="TextFaster"></a><h3> <span class="mw-headline"> TextFaster </span></h3>
<p>TextFaster is a keyboard with keys six times bigger than the usual keyboard. 
</p><p>Launch the application with url and it will open. 
</p>
<pre>NSString *stringURL = @"keyboard://?text=Hello%20World&amp;fromapp=mailto";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Threadnote" id="Threadnote"></a><h3> <span class="mw-headline"> Threadnote </span></h3>
<p>This information was provided by Bryan Clark from the Threadnote team!
</p><p><a href="http://threadnote.com/" class="external text" title="http://threadnote.com/" rel="nofollow">Threadnote</a> is a tweet-like private notes app for the iPhone.
</p>
<pre>NSString *stringURL = @"threadnote://new?text=hello";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>This URL would make a new note with the text "hello".
</p>
<a name="TikiSurf" id="TikiSurf"></a><h3> <span class="mw-headline"> TikiSurf </span></h3>
<p>TikiSurf is a mobile browser with fast access to  websites, maps, apps through a visual touch panel.
</p><p>Launch the application alone or with a collection url to open it at startup. 
</p><p>Application alone:
</p>
<pre>NSString *stringURL = @"tikisurf://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>Launch a collection from url:
</p><p><a href="http://prod1.tikilabs.com/pushContent/iphone/annuaireUnivers/" class="external text" title="http://prod1.tikilabs.com/pushContent/iphone/annuaireUnivers/" rel="nofollow">Full list of existing collections</a>
</p><p><a href="http://www.tikisurf.fr/factoryEN.php" class="external text" title="http://www.tikisurf.fr/factoryEN.php" rel="nofollow">Tool to create your own collection</a>
</p>
<pre>NSString *stringURL = @"tikisurf://activateGrammar?url=http://get.tikilabs.com/grammars/rzi10d8292gwy0fj.html";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<a name="Waze" id="Waze"></a><h3> <span class="mw-headline"> Waze </span></h3>
<p><a href="http://www.waze.com" class="external free" title="http://www.waze.com" rel="nofollow">http://www.waze.com</a>
</p><p>App Store: <a href="http://itunes.apple.com/us/app/id323229106?mt=8&amp;ign-mpt=uo%3D6" class="external free" title="http://itunes.apple.com/us/app/id323229106?mt=8&amp;ign-mpt=uo%3D6" rel="nofollow">http://itunes.apple.com/us/app/id323229106?mt=8&amp;ign-mpt=uo%3D6</a>
</p>
<pre>NSString *stringURL = @"waze://?ll=37.331689,-122.030731&amp;z=10";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>examples:
show on map:
waze://?ll=37.331689,-122.030731&amp;z=10
</p><p>navigate:
waze://?ll=37.331689,-122.030731&amp;navigate=yes
</p><p>options:
search for address:
q=&lt;address search term&gt;
</p><p>center map to lat / lon:
ll=&lt;lat&gt;,&lt;lon&gt;
</p><p>set zoom (minimum is 6):
z=&lt;zoom&gt;
</p><p>auto start navigation:
navigate=yes
</p>
<a name="Webmail.2B" id="Webmail.2B"></a><h3> <span class="mw-headline"> Webmail+ </span></h3>
<p><a href="http://itunes.apple.com/us/app/webmail/id392501588?mt=8" class="external free" title="http://itunes.apple.com/us/app/webmail/id392501588?mt=8" rel="nofollow">http://itunes.apple.com/us/app/webmail/id392501588?mt=8</a>
</p>
<pre>NSString *stringURL = @"wpp://account/UNIQUE_ID";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>where 'UNIQUE_ID' is the ID of your account.  This can be obtained in the settings of the application under Push Notifications.  This will launch my application and immediately load the webmail account specified by the ID.
</p>
<a name="Where_To.3F" id="Where_To.3F"></a><h3> <span class="mw-headline"> Where To? </span></h3>
<p>This link is courtesy of Orwin Gentz from FutureTap!
</p>
<pre>NSURL *whereToUrl = [NSURL URLWithString:@"whereto://?search=Bars"];
[[UIApplication sharedApplication] openURL:whereToUrl];
</pre>
<p>The URL scheme is <tt>whereto</tt> (or <tt>whereto5</tt> to require Where To? 5.0 or higher). The following GET query parameters are supported:
</p>
<ul><li> <tt>search</tt>: Sets the category or name to be searched. Equivalent to starting a manual search using the Search button within the app. Notice that Where To? automatically resolves (and translates if necessary) built-in categories for optimized search quality. For instance, the provided example value is dynamically translated to “Öffentl. Verkehrsmittel” when the user’s interface language is German.
</li><li> <tt>location</tt>: Sets the location using latitude, longitude to be searched at. The example value sets the location to Apple’s HQ in Cupertino. Requires the search parameter to be set. If location is not specified, Where To? will search at the current location.
</li><li> <tt>poi</tt>: Lets Where To? display a specific place. The example value will display the Apple Store in San Francisco. Not to be combined with search and location parameters!
</li></ul>
<p>More information at <a href="http://www.futuretap.com/api/" class="external text" title="http://www.futuretap.com/api/" rel="nofollow">the Where To? API page</a>
</p>
<a name="iWavit_Tabula_Rasa" id="iWavit_Tabula_Rasa"></a><h3> <span class="mw-headline"> iWavit Tabula Rasa </span></h3>
<p>Tabula Rasa is a Universal Remote Control Editor app which is used to create "Virtual Remotes" that you can use to control your IR Devices (TV, DVD..) and Computers (PC, Mac, Linux).  The app requires the iWavit hardware accessory which adds new hardware capabilities to the iPhone, iPad, and iPod touch (<a href="http://www.iwavit.com" class="external free" title="http://www.iwavit.com" rel="nofollow">http://www.iwavit.com</a>).
</p><p>Launch the application alone with this call
</p>
<pre>NSString *stringURL = @"wavittabularasa://";
NSURL *url = [NSURL URLWithString:stringURL];
[[UIApplication sharedApplication] openURL:url];
</pre>
<p>There are many other iWavit apps (&gt;30), that tend to have names like iWavit PS3 or iWavit DirecTV, which are apps dedicated to controlling specific major brand electronics devices.  These apps all tend to follow the same URL Schemes:
@"wavitxxx://"
where xxx is the name after the iWavit, all lower case.
</p>

		</div>


<br>
The content on this page is available under <a href="http://creativecommons.org/licenses/by-nc-sa/3.0/" class="external " title="http://creativecommons.org/licenses/by-nc-sa/3.0/">Attribution-Noncommercial-Share Alike 3.0 Unported</a>.
<br><a href="http://creativecommons.org/licenses/by-nc-sa/3.0/"><img src="http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png" alt="Attribution-Noncommercial-Share Alike 3.0 Unported"></a>

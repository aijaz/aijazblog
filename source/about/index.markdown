---
layout: page
title: "About Me"
date: 2015-02-03 00:00
comments: false
sharing: true
footer: true
---

<!-- ai l /images/aijaz.jpg /images/aijaz.jpg 235 256 Hello! -->
As a hacker by nature, I enjoy making things.  I'm the developer and
designer behind the [Qur'an Memorizer](http://quranmemorizer.com)
iOS app, and am currently working as an iOS developer at
[Vaporstream](http://www.vaporstream.com).  I [teach iOS development on the weekends](http://euclidte.ch/) to children and adults in the community. I also dabble in woodworking
and origami and making tangible things with my hands. Oh, I've also
written  a pretty nifty open-source job scheduler called
[TaskForest](http://www.taskforest.com). For those who're interested, I've also put up 
a <a href="#work">somewhat detailed account of my work experience</a> towards the bottom of this page. 

I'm a student of Wing Chun Kung Fu, having studied at the excellent
[Philip Nearing School of Wing Chun](http://wingchunchicago.com/).  I've
also [made a Kung Fu wooden dummy](/2012/07/15/making-a-kung-fu-wooden-dummy/) (what a fun project!).  

You can find me here:

* [This blog](http://www.aijazansari.com/)
* [Twitter](http://twitter.com/_aijaz_)
* Email - &lt;myfirstname&gt; at &lt;myfirstname&gt; dot net
* [Linked In](http://www.linkedin.com/in/aijaz)
* [GitHub](http://github.com/aijaz)

## Licenses

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc-nd/3.0/88x31.png" /></a>

Almost all photographs found on this blog were taken by me and are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.

Those photographs not taken by me are attributed accordingly.  If you
find any image that is not attributed, please email me and let me know.


<a name="work"></a>
# Work Experience

## Software Developer, FastModel Sports (August 2015 - Present)

I just started working at FastModel as an iOS developer. I will be working on iOS apps for coaches and recruiters.

## Principal Engineer, Vaporstream (2012 - August 2015)

I was the sole iOS developer of secure messaging and social networking
apps.  When I left, Vaporstream was a small startup with 5 developers: 1
each for iOS, Android, and the server as well a QA and a DevOps
engineer. As a senior engineer, I participated in requirements gathering,
release planning, API design and prototyping for feasability
determination. I've got one patent application under my name. I know
that's not much, but I'm pretty proud of it. Representing Vaporstream, I
spoke at the Snow-Mobile conference in Madison, WI in 2012, advising
mobile developers on what to do after they ship their app. I also became
a mentor at [MobileMakers Chicago](http://http://www.mobilemakers.co/). By
the time I left Vaporstream I had mentored 7 cohorts of students (14
students in all) and continue to mentor some of them after they started
their careers as iOS developers.

Some key technologies that I worked with a lot are: 

- AVFoundation (live video views with overlays and live filters)
- WebSockets
- AFNetworking
- The iOS KeyChain
- SQLite3 (with FMDB, because [I'm not a savage](/2013/05/06/evolution-of-objc-database-code/), you know)

I also developed two python apps that are critical to the continuing
operations of the business. The first is VaporBot. This is a chat bot with
which users can communicate. VaporBot runs the classic 'Eliza' program and
has humorous conversations with clients as they're testing the app.

The second python app that I wrote is called HealthBot. This is an app
that pretends to be a client and has conversations with VaporBot. If the
attempt at having the two bots speak to each other fails at any time,
HealthBot can alert the monitoring systems and let us know exactly what is
wrong. For example, it can determine if the problem is with messaging,
talking to VaporBot or even connection to our cloud storage of images.  


## Principal, Euclid Software, LLC. (2010 - Present)

Euclid Software is the company I started when I wanted to register as an iOS developer and submit my first app, [Qur'an Memorizer](http://quranmemorizer.com), to the App Store. Qur'an Memorizer has been my most popular app so far. Along with the free variants there have been more than 680,000 downloads worldwide. The sales, while not enough to live off, are enough to pay for my computers, books, and associated toys, so I can confidently say that this app (and the community I built around it) has been a resounding success. The most beloved features of this app are the photo-realistic interface, the page-turning affordances and (to my surprise) the bookmarking feature. 

I've also started working on a promising new app called [Pix2Doc](http://pix2doc.com). We're still in a public beta stage before we fine tune our app and announce our services to the public. Every so often I'll publish apps for clients. They have asked that I not name the apps that I've worked on, and those apps are almost as complex as the apps I work on at Vaporstream.

Through Euclid I've also started a service called [EuclidTech](http://euclidte.ch) where I teach neighborhood children and adults iOS development. For our first 16-week course we wrote the [Robots](https://github.com/aijaz/Robots) game. When looking at this code, please keep in mind that this code was written as an instructional exercise for 12- and 15-year old children learning how to program. 

I'm a frequent speaker at CocoaHeads Chicago and gave a lightning talk on hashing and salting passwords at SecondConf in 2012. I was also a backup speaker at SecondConf 2013. 

## Senior Developer, Citadel (2005 - 2012)

At Citadel I wrote Perl code for the Global Middle and Back Office department. Our code ran exclusively on Linux and was responsible for obtaining data feeds from a variety of vendors, parsing them, normalizing them, and loading them into our internal databases. My clients were the traders and quants who would access the data to make trading decisions. I did not work in the High Frequency Trading group, so our code took minutes to run, as opposed to seconds. During my time there we ran about 1,500 jobs per day. I wrote an internal web app that would allow us to track job failures and then note what was done to resolve the problem. This was critical for becoming SAS-70 compliant, as well as for helping whoever was on call to address the issue without having to wake anyone else; they could simply read the notes from the last incident. I also wrote other web apps for monitoring Mortgage Remittance data feeds.  

Citadel is where I learned how to work on fast-moving projects where requirements could change with little or no notice (due to external and market factors). I also learned the importance of the '3 AM test': My code would have to make sense to the on-call person who may be reading it for the first time at 3:00 am, having been woken up because, say, opening prices for London are not available. This is not the place for overly clever code, because if a sleepy coworker cannot understand it and modify it, he or she would be calling you up to fix it yourself.

Most of our programs would write to one or more Sybase databases. At Citadel I became very proficient with SQL as well as managing and mantaining database connections in code. 

## President, ENoor Creations (1998 - 2005)

At ENoor I started off running web hosting service, and then migrated to writing web apps for clients in Apache/mod_perl. During the 7 years that ENoor was operating I focused on various industries, including Law Enforcement, Automobile Sales, Telecom, Printing, and Non-Profits. One of my most interesting projects was customizing an SMTP server for a client, to implement their authentication scheme. I wound up writing authentication code in C for the ```qmail-smtp``` mail server.  ENoor is also where I developed [TaskForest](http://taskforest.com), an open source job scheduler, to manage my own machines. 

I also spent a decent amount of time over a three-year period writing software for Webvestor. This was a trading company started by a pair of traders at the Chicago Mercantile Exchange. I wrote an app in Visual C++ that would use the FIX API to post data from their local trading applications to the CME. Since they had a dedicated wired connection to the CME, any such app would have to be audited and certified by the Exchange. Working on this and getting the app certified was a milestone in my growth as a Software Developer. 

## Lead Engineer, Motorola (1995 - 1999)

At Motorola I had the good fortune to work on a product during the last few years of its lifespan (The IS-41 Converter) as well as work on a product as it was being developed for the first time (The Visitor's Location Register, or VLR).  I started off with the IS-41 converter working on their User Interface team and then worked on authentication software required by the Department of Justice. When the VLR was announced I participated in the creation of Software Functional Specifications, Design Documentation and finally the C++ code itself.  

I was responsible for the Call Processing code for two key features: Message Waiting Notification and Local Number Portability. I also wrote an internal tool called 'Cogent' that read in simple requirements for different kinds of Call State Models and generated the C++ code for the individual CSM subclasses. This became part of the build phase and eliminated the possibility of developer error introducing inconsistencies between subclasses. 

Because the VLR application ran on a diskless board on Motorola's telecom switches, we needed a low-footprint in-memory database management system to handle records for the call state models of the people who were currently making phone calls. I was in charge of this 'dbmgmt' system, and implemented it using height-balanced binary trees in C++.  




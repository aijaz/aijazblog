---
layout: page
title: Categories
footer: false
---

<div class=category_list>
    <ul>
        <li><a href="/categories/30days/">30days</a></li>
        <li><a href="/categories/camping/">Camping</a></li>
        <li><a href="/categories/computers/">Computers</a></li>
        <li><a href="/categories/diy/">DIY</a></li>
        <li><a href="/categories/design/">Design</a></li>
        <li><a href="/categories/editorial/">Editorial</a></li>
        <li><a href="/categories/fitness/">Fitness</a></li>
        <li><a href="/categories/history/">History</a></li>
        <li><a href="/categories/knots/">Knots</a></li>
        <li><a href="/categories/opinion/">Opinion</a></li>
        <li><a href="/categories/photography/">Photography</a></li>
        <li><a href="/categories/photos/">Photos</a></li>
        <li><a href="/categories/reviews/">Reviews</a></li>
        <li><a href="/categories/woodworking/">Woodworking</a></li>
    </ul>
</div>
